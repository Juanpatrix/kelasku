# KelasKu

<p align="center"><img width=12.5% src="https://gitlab.com/Juanpatrix/kelasku/-/raw/master/public/Logo.png"></p>

KelasKu is PERN-based, web that connects teachers with parents to monitor the condition of children's learning development as well as online children's learning classes.

## Members:
* Juan Patrick 1906355516
* Ibrahim Malik Fitrasani 1906355560
* Samuel Nathaniel Halim 1906355560

## Features:

* Login (Teacher, Parent, Student)

### Dashboard System

Dashboard is a feature that provides information about current study and monitors learning activities.

1 Teacher:
  * Information about the latest material
  * Information about the fastest uploads
  * Latest agenda information
  * Button shortcut to access Agenda

2 Parent:
  * Information about the latest material
  * Ranking his/her child
  * Graph score
  * Latest agenda information

3 Student:
  * Information about the latest material
  * Ranking
  * Graph score
  * Current tack

### Class

Class is a feature that accommodates children's learning facilities at school.

1 Teacher:
  * Access class by subject
  * Access materials and assignments
  * Upload assignment link
  * Upload material link
  * Assignment correction
  * See student answer link

2 Parent:
  * Access class by subject
  * Access materials and assignments
  * See the child's score

3 Student:
  * Access classes by subject
  * Access materials and assignments
  * Submit answer link
  * Access task

Note: the submission assignment is opened as long as the teacher does not correct the assignment. If the assignment has been corrected, the submission was close

### Leaderboard

Feature that displays data on the ranking of children in class. This feature can display the whole by calculating the value and dividing the value per subject or the whole subject

### Attendance

Feature to monitor the child's attendance status in class

1 Teacher:
  * Access attendance by subject
  * Creating attendance

2 Parent:
  * Access attendance by his/her own child

### Agenda

Feature that is a means for teachers to communicate with parents to discuss children's learning development in class.

1 Teacher:
  * Access agenda by category
  * Access more detail about agenda
  * Making Agenda

2 Parent:
  * Access agenda by category
  * Access more detail about agenda

List Categories:
  * Informasi
  * Pembayaran
  * Perilaku
  * Tugas
  * Ujian
  * Lainnya
