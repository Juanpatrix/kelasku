module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {
      backgroundColor: ['active', 'checked'],
      backgroundImage: ['active', 'checked'],
      borderColor: ['active', 'checked'],
      textColor: ['active', 'checked'],
      ringOffsetWidth: ['active', 'checked'],
      ringWidth: ['active', 'checked'],
      ringColor: ['active', 'checked'],
      ringOffsetColor: ['active', 'checked'],
      ringOpacity: ['active', 'checked'],
      outline: ['active', 'checked'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
