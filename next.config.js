const path = require('path');

module.exports = {
  eslint: {
    ignoreDuringBuilds: true,
  },
  images: {
    domains: ['as1.ftcdn.net','cdn.pixabay.com'],
  },
  reactStrictMode: true,
  resolve: {
    alias: {
      '@/styles/*': path.resolve('./src/styles/*'),
      '@/layouts/*': path.resolve('./src/layouts/*'),
      '@/components/*': path.resolve('./src/components/*'),
      '@/containers/*': path.resolve('./src/containers/*'),
      '@/redux/*': path.resolve('./src/redux/*'),
      '@/images/*': path.resolve('./public/*'),
      '@/favicon/*': path.resolve('./public/favicon/*'),
    },
  },
};
