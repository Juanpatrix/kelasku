import { callAPI } from '../helpers/network';

export default async function Leaderboard(token, id) {
  const data = await callAPI({
    url: `/leaderboards?subject=${id}`,
    method: 'GET',
    headers: {
      'content-type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}
