import { callAPI } from '../helpers/network';

function Attendance() {
  const fetchAttendanceBySubject = async (token, id) => {
    const data = await callAPI({
      url: `/attendances/teachers/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAttendanceParent = async (token) => {
    const data = await callAPI({
      url: '/attendances/parents',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const postAttendance = async (token, values, id) => {
    const data = await callAPI({
      url: `/attendances/${id}`,
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(values),
    });
    return data;
  };

  const fetchStudent = async (token) => {
    const data = await callAPI({
      url: '/students',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  return {
    fetchAttendanceBySubject,
    fetchAttendanceParent,
    fetchStudent,
    postAttendance,
  };
}

export default Attendance();
