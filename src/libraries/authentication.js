import { callAPI } from '../helpers/network';

export default async function Authentication(token) {
  const data = await callAPI({
    url: '/auth',
    method: 'GET',
    headers: {
      'content-type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    },
  });
  return data.data;
}
