import { callAPI } from '../helpers/network';

function Agenda() {
  const fetchAllAgenda = async (token) => {
    const data = await callAPI({
      url: '/agenda',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAgendaByParent = async (token) => {
    const data = await callAPI({
      url: '/parentagenda/my',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAgenda = async (token, id) => {
    const data = await callAPI({
      url: `/agenda/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAgendaParent = async (token, id) => {
    const data = await callAPI({
      url: `/parentagenda/my/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchStudent = async (token) => {
    const data = await callAPI({
      url: '/students',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const postAgenda = async (token, data) => {
    const result = await callAPI({
      url: '/agenda',
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data,
    });
  };

  return {
    fetchAllAgenda,
    fetchAgenda,
    fetchAgendaByParent,
    fetchAgendaParent,
    fetchStudent,
    postAgenda,
  };
}

export default Agenda();
