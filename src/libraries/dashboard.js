import { callAPI } from '../helpers/network';

function Dashboard() {
  const fetchReadingLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/materials',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAssignmentLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/assignments',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAgendaLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/agenda',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAgendaTeacherLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/teacher/agenda',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchStudent = async (token) => {
    const data = await callAPI({
      url: '/dashboard/child',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchScoreLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/scores',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchScoreStudentLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/student/scores',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchSubmitLatest = async (token) => {
    const data = await callAPI({
      url: '/dashboard/submission',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };


  return {
    fetchAgendaLatest,
    fetchAgendaTeacherLatest,
    fetchAssignmentLatest,
    fetchReadingLatest,
    fetchScoreLatest,
    fetchScoreStudentLatest,
    fetchStudent,
    fetchSubmitLatest,
  };
}

export default Dashboard();
