import { callAPI } from '../helpers/network';

function User() {
  const fetchUser = async (token) => {
    const data = await callAPI({
      url: '/users/current-user',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const login = async (data) => {
    const result = await callAPI({
      url: '/signin',
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=utf-8',
      },
      data,
    });
    return result;
  };

  return {
    fetchUser,
    login,
  };
}

export default User();
