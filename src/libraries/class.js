import { callAPI } from '../helpers/network';

function Class() {
  const fetchSubject = async (token) => {
    const data = await callAPI({
      url: '/subjects',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchSubjectById = async (token, id) => {
    const data = await callAPI({
      url: `/subjects/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchReadingBySubject = async (token, id) => {
    const data = await callAPI({
      url: `/materials/subject/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAssignmentBySubject = async (token, id) => {
    const data = await callAPI({
      url: `/subject/${id}/assignments`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAssignmentParent = async (token, id) => {
    const data = await callAPI({
      url: `/parentassignments/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchAssignmentStudent = async (token, id) => {
    const data = await callAPI({
      url: `/studentassignments/my/${id}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const PostAssignmentBySubject = async (token, values) => {
    const data = await callAPI({
      url: '/assignments',
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(values),
    });
    return data;
  };

  const PostReadingBySubject = async (token, values) => {
    const data = await callAPI({
      url: '/materials',
      method: 'POST',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(values),
    });
    return data;
  };

  const fetchAssignmentStudentBySubject = async (token, id, idAsg) => {
    const data = await callAPI({
      url: `/subject/${id}/assignments/${idAsg}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const fetchStatusAssignment = async (token, id, idAsg) => {
    const data = await callAPI({
      url: `/studentassignments/${id}/assignments/${idAsg}`,
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  };

  const updateAnswer = async (token, values, id) => {
    const data = await callAPI({
      url: `/studentassignments/answer/${id}`,
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(values),
    });
    return data;
  };

  const updateScore = async (token, values) => {
    const data = await callAPI({
      url: '/studentassignments/score',
      method: 'PUT',
      headers: {
        'content-type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(values),
    });
    return data;
  };

  return {
    fetchAssignmentBySubject,
    fetchAssignmentStudent,
    fetchAssignmentStudentBySubject,
    fetchAssignmentParent,
    fetchStatusAssignment,
    fetchSubject,
    fetchSubjectById,
    fetchReadingBySubject,
    PostAssignmentBySubject,
    PostReadingBySubject,
    updateAnswer,
    updateScore,
  };
}

export default Class();
