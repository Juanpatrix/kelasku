import axios from 'axios';

export const BASE_URL = 'https://kelasku-backend-engine.herokuapp.com';

export const callAPI = async (config) => {
  try {
    Object.assign(config, {
      baseURL: BASE_URL,
    });
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return Promise.reject(error);
  }
};
