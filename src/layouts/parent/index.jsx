import { useState } from 'react';
import {
  BookOpenIcon, HomeIcon, ChartBarIcon, ClipboardListIcon, ChatAltIcon,
} from '@heroicons/react/solid';
import Navbar from '@/components/Navbar';
import Sidebar from '@/components/Sidebar';

const navigation = [
  { icon: <HomeIcon className="w-8 h-8 mr-3" />, name: 'Beranda', href: '/parent' },
  { icon: <BookOpenIcon className="w-8 h-8 mr-3" />, name: 'Kelas', href: '/parent/subject' },
  { icon: <ClipboardListIcon className="w-8 h-8 mr-3" />, name: 'Absensi', href: '/parent/attendance' },
  { icon: <ChartBarIcon className="w-8 h-8 mr-3" />, name: 'Papan Peringkat', href: '/parent/leaderboard' },
  { icon: <ChatAltIcon className="w-8 h-8 mr-3" />, name: 'Agenda', href: '/parent/agenda' },
];

export default function ParentLayout(props) {
  const [value, setValue] = useState(props.page);
  return (
    <div className="min-h-screen bg-gray-50">
      <Navbar page={value} itemNav={navigation} />
      <div className="flex">
        <Sidebar page={value} itemNav={navigation} />
        <div className="w-full">
          {props.children}
        </div>
      </div>
    </div>
  );
}
