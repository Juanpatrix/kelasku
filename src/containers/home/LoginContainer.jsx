import { Formik, Field } from 'formik';
import Image from 'next/image';
import * as Yup from 'yup';
import { EyeIcon, EyeOffIcon } from '@heroicons/react/solid';
import { Dialog, Transition } from '@headlessui/react';
import { Fragment, useState } from 'react';
import { useRouter } from 'next/router';
import User from '../../libraries/user';

const validationSchema = Yup.object({
  username: Yup.string().required('Harap Isi Username'),
  password: Yup.string().required('Harap Isi Kata Sandi'),
});

const initialValues = {
  username: '',
  password: '',
};
export default function LoginContainer() {
  const router = useRouter();
  const [state, setState] = useState({
    loading: false,
    passwordShow: false,
    message: '',
    open: false,
  });
  const closeModal = () => {
    setState({...state, open: false});
  };

  const openModal = () => {
    setState({...state, open: true});
  };

  const handleOnSubmit = async (values, {resetForm}) => {
    setState({...state, loading: true});
    try {
      const data = await User.login(values);
      setTimeout(() => {
        setState({...state, loading: false});
        localStorage.setItem('user', data.token);
        router.replace('/loading');
      }, 1000);
    } catch (error) {
      localStorage.removeItem('user');
      setState({...state, message: 'Username/kata sandi salah. Periksa kembali username/kata sandi anda.'});
      resetForm(initialValues);
    }
  };
  const togglePassword = () => {
    setState({...state, passwordShow: !state.passwordShow});
  };
  return (
    <>
      <div className="md:flex md:flex-row-reverse w-full h-full">
        {/* Section Form */}
        <div className="md:w-1/2 h-screen bg-gray-100 my-auto">
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            validationSchema={validationSchema}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
            }) => (
              <form className="relative p-10 transform translate-y-16" onSubmit={handleSubmit}>
                {/* Section Logo */}
                <div className="mt-2 mb-10 text-center">
                  <h1 className="text-3xl font-bold">Halaman Login</h1>
                </div>
                {/* Section Logo */}
                <div>
                  <label className="flex flex-col" htmlFor="username">
                    <span className="text-base font-medium mb-2">Username</span>
                    <Field
                      required
                      name="username"
                      id="username"
                      type="text"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="p-2 rounded border-none focus:border-none outline-none focus:outline-none focus:ring-2 focus:ring-opacity-50 focus:ring-yellow-400"
                      placeholder="Username Anda"
                    />
                  </label>
                  <div className={`text-red-400 ${touched.username && errors.username ? 'error' : ''}`}>
                    {touched.username && errors.username && errors.username}
                  </div>
                </div>
                <div className="my-5">
                  <label className="flex flex-col" htmlFor="password">
                    <span className="text-base font-medium mb-2">Kata Sandi</span>
                    <div className="flex flex-row-reverse items-center">
                      <Field
                        required
                        name="password"
                        id="password"
                        type={state.passwordShow ? 'text' : 'password'}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className="w-full p-2 rounded border-none focus:border-none outline-none focus:outline-none focus:ring-2 focus:ring-opacity-50 focus:ring-yellow-400"
                        placeholder="Kata Sandi Anda"
                      />
                      <button className="w-6 absolute mr-3" type="button" onClick={togglePassword}>{state.passwordShow ? <EyeIcon /> : <EyeOffIcon /> }</button>
                    </div>
                  </label>
                  <div className={`text-red-400 ${touched.password && errors.password ? 'error' : ''}`}>
                    {touched.password && errors.password && errors.password}
                  </div>
                </div>
                <div className="message ml-2 text-red-400 text-sm mb-12 text-left flex gap-2">
                  {state.message}
                </div>
                <div className="flex mt-14 mx-5 items-center">
                  <div className="text-left w-full">
                    <button type="button" className="w-20" onClick={openModal}>Buat Akun</button>
                  </div>
                  <div className="text-center w-1/3 md:w-2/5">
                    <button type="submit" className="bg-yellow-400 w-full hover:bg-yellow-300 rounded py-2 text-white" disabled={state.loading}>
                      {state.loading ? 'Harap Tunggu...' : 'Masuk'}
                    </button>
                  </div>
                </div>
              </form>
            )}
          </Formik>
        </div>
        {/* Section Form */}
        {/* Section Background */}
        <div className="bg-gray-900 relative md:min-h-screen h-0 md:w-1/2">
          <Image
            alt="Login"
            src="/images/class.png"
            layout="fill"
            objectFit="cover"
            className=""
          />
        </div>
        {/* Section Background */}
      </div>
      <Transition appear show={state.open} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto bg-opacity-70 bg-black"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900"
                >
                  Pendaftaran Online
                </Dialog.Title>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">
                    Kontak: kelasku@gmail.com
                  </p>
                </div>

                <div className="mt-4">
                  <button
                    type="button"
                    className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                    onClick={closeModal}
                  >
                    Tutup
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
