import React from 'react';
import {render, fireEvent, screen} from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import LoginContainer from '../LoginContainer';

describe('Login Testing', () => {
  beforeEach(() => {
    render(
      <LoginContainer />,
    );
  });

  test('login page have label Username and Password', () => {
    const label1 = screen.getByText('Username');
    const label2 = screen.getByText('Kata Sandi');
    expect(label1).toBeInTheDocument();
    expect(label2).toBeInTheDocument();
  });

  test('Input username and Password', async () => {
    const inputUsername = screen.getByPlaceholderText('Username Anda');
    const inputPassword = screen.getByPlaceholderText('Kata Sandi Anda');
    await act(async () => {
      fireEvent.click(inputUsername);
      fireEvent.change(inputUsername, { target: {value: 'guest'}});
      fireEvent.click(inputPassword);
      fireEvent.change(inputPassword, { target: {value: 'password' } });
    });
    expect(inputUsername).toHaveValue('guest');
    expect(inputPassword).toHaveValue('password');
  });

  test('Validation Email', async () => {
    const inputUsername = screen.getByPlaceholderText('Username Anda');
    await act(async () => {
      fireEvent.click(inputUsername);
      fireEvent.change(inputUsername, { target: {value: ''}});
      fireEvent.blur(inputUsername);
    });
    const value = await screen.getByText('Harap Isi Username');
    expect(value).toBeInTheDocument('Harap Isi Username');
  });

  test('Validation Password', async () => {
    const inputPassword = screen.getByPlaceholderText('Kata Sandi Anda');
    await act(async () => {
      fireEvent.click(inputPassword);
      fireEvent.change(inputPassword, { target: {value: ''}});
      fireEvent.blur(inputPassword);
    });
    const value = await screen.getByText('Harap Isi Kata Sandi');
    expect(value).toBeInTheDocument('Harap Isi Kata Sandi');
  });
});
