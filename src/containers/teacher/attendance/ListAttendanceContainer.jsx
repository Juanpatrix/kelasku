import Link from 'next/link';
import { useRouter } from 'next/router';
import { PencilAltIcon, PlusIcon } from '@heroicons/react/solid';
import { useEffect, useState } from 'react';
import Attendance from '../../../libraries/attendance';
import BackButton from '@/components/BackButton';

export default function ListAttendanceContainer() {
  const [list, setList] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const loadAttendance = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemAttendance = await Attendance.fetchAttendanceBySubject(token, id);
        setList(itemAttendance);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadAttendance();
  }, [router.isReady]);
  return (
    <div className="m-4">
      <div className="w-full justify-between mt-12 flex">
        <BackButton />
        <Link
          href={{
            pathname: '/teacher/attendance/[id]/post',
            query: { id },
          }}
        >
          <a>
            <button type="button" className="bg-yellow-400 hover:bg-yellow-300 flex text-white px-5 items-center mr-10 text-sm font-semibold py-3 rounded">
              <PlusIcon className="h-5 w-5 mr-5" />
              Buat Absensi
            </button>
          </a>
        </Link>
      </div>
      <table className="table-fixed mx-auto text-center w-full mt-16">
        <thead>
          <tr className="h-10 bg-blue-300 border-t border-black">
            <th className="w-1/4 md:text-base text-sm">Pertemuan</th>
            <th className="w-1/2 md:text-base text-sm text-center">Tanggal</th>
            <th className="w-1/4 md:text-base text-sm">Hadir</th>
            <th className="w-1/4 md:text-base text-sm">Sakit</th>
            <th className="w-1/4 md:text-base text-sm">Izin</th>
            <th className="w-1/4 md:text-base text-sm">Alpha</th>
            {/* <th className="w-1/4 md:text-base text-sm" /> */}
          </tr>
        </thead>
        <tbody>
          {
            list.length !== 0 ? list.map((obj, position) => (
              <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                <td className="md:text-lg text-xs">{`Pertemuan ${position + 1}`}</td>
                <td className="md:text-lg text-xs">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</td>
                <td className="md:text-lg text-xs">{obj.hadir}</td>
                <td className="md:text-lg text-xs">{obj.sakit}</td>
                <td className="md:text-lg text-xs">{obj.izin}</td>
                <td className="md:text-lg text-xs">{obj.alpha}</td>
                {/* <td className="md:text-lg text-xs"><Link href="#"><button className="bg-yellow-500 text-white p-1 md:p-2 rounded" type="button"><PencilAltIcon className="h-5 w-5" /></button></Link></td> */}
              </tr>
            )) : (
              <tr>
                <td colSpan="6" className="text-2xl font-bold h-52">Data kehadiran belum tersedia</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </div>
  );
}
