import { useState, useEffect } from 'react';
import { RadioGroup } from '@headlessui/react';
import { Formik, Field } from 'formik';
import { useRouter } from 'next/router';
import Attendance from '../../../libraries/attendance';
import BackButton from '@/components/BackButton';

export default function AttendanceContainer() {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const [initial, setInitial] = useState({});
  const [members, setMember] = useState([{id: '', name: ''}]);
  const { id } = router.query;
  const handleOnSubmit = async (values, {resetForm}) => {
    setLoading(true);
    try {
      const status = Object.values(values);
      const transform = status.map((obj, position) => ({
        student_id: members[position].id,
        status: obj,
      }));
      const token = localStorage.getItem('user');
      Attendance.postAttendance(token, transform, id);
      setTimeout(() => {
        setLoading(false);
        router.back();
      }, 1000);
    } catch (error) {
      router.back();
    }
  };
  const loadMember = async () => {
    try {
      const token = localStorage.getItem('user');
      const students = await Attendance.fetchStudent(token);
      const obj = {};
      for (let i = 0; i < students.length; i++) {
        obj[students[i].id] = 'H';
      }
      setInitial(obj);
      setMember(students);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadMember();
  }, [router.isReady]);
  const date = new Date();
  return (
    <div className="m-4">
      <div className="mt-12">
        <BackButton />
        <p className="text-center font-semibold text-3xl">
          Tanggal
          {' '}
          {date.toDateString()}
        </p>
      </div>
      <Formik
        initialValues={initial}
        onSubmit={handleOnSubmit}
        enableReinitialize
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          touched,
          errors,
        }) => (
          <form className="mt-10" onSubmit={handleSubmit}>
            <table className="table-fixed w-11/12 mx-auto text-center mt-16">
              <thead>
                <tr className="h-14 bg-blue-300 border-t border-black">
                  <th className="w-1/12 text-lg text-center">No</th>
                  <th className="w-1/6 text-lg text-center">Nama</th>
                  <th className="w-1/4 text-lg">Hadir</th>
                  <th className="w-1/4 text-lg">Sakit</th>
                  <th className="w-1/4 text-lg">Izin</th>
                  <th className="w-1/4 text-lg">Alpha</th>
                </tr>
              </thead>
              <tbody>
                {members.map((obj, position) => (
                  <tr key={position} role="group" className="h-20 bg-blue-100" aria-labelledby="radio-group">
                    <td>
                      {position + 1}
                    </td>
                    <td className="font-medium text-left pl-3">
                      {obj.name}
                    </td>
                    <td>
                      <label className="flex items-center justify-center">
                        <Field type="radio" name={obj.id} value="H" className="form-radio border-0 ring-offset-0 h-12 w-12 hover:bg-blue-50 bg-blue-200 text-yellow-300 checked:ring-0 active:ring-0 checked:bg-none active:bg-blue-300 active:text-yellow-400" />
                        <div className="z-10 absolute font-semibold text-lg">H </div>
                      </label>
                    </td>
                    <td>
                      <label className="flex items-center justify-center">
                        <Field type="radio" name={obj.id} value="S" className="form-radio border-0 ring-offset-0 h-12 w-12 hover:bg-blue-50 bg-blue-200 text-yellow-300 checked:ring-0 active:ring-0 checked:bg-none active:bg-blue-300 active:text-yellow-400" />
                        <div className="z-10 absolute font-semibold text-lg">S </div>
                      </label>
                    </td>
                    <td>
                      <label className="flex items-center justify-center">
                        <Field type="radio" name={obj.id} value="I" className="form-radio border-0  ring-offset-0 h-12 w-12 hover:bg-blue-50 bg-blue-200 text-yellow-300 checked:ring-0 active:ring-0 checked:bg-none active:bg-blue-300 active:text-yellow-400" />
                        <div className="z-10 absolute font-semibold text-lg">I </div>
                      </label>
                    </td>
                    <td>
                      <label className="flex items-center justify-center">
                        <Field type="radio" name={obj.id} value="A" className="form-radio border-0 ring-offset-0 h-12 w-12 hover:bg-blue-50 bg-blue-200 text-yellow-300 checked:ring-0 active:ring-0 checked:bg-none active:bg-blue-300 active:text-yellow-400" />
                        <div className="z-10 absolute font-semibold text-lg">A </div>
                      </label>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="text-center mt-14">
              <button className="bg-yellow-400 px-4 py-2 w-1/3 text-white hover:bg-yellow-300" type="submit" disabled={loading}>{loading ? 'Sedang Diproses...' : 'Submit'}</button>
            </div>
          </form>
        )}
      </Formik>
    </div>

  );
}
