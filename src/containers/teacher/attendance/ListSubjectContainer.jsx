import Link from 'next/link';
import { useEffect, useState } from 'react';
import { PencilAltIcon } from '@heroicons/react/solid';
import Class from '../../../libraries/class';

export default function ListSubjectContainer() {
  const [subject, setSubject] = useState(false);
  const loadSubject = async () => {
    try {
      const token = localStorage.getItem('user');
      const itemSubject = await Class.fetchSubject(token);
      setSubject(itemSubject);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadSubject();
  }, []);
  return (
    <div className="m-4">
      <table className="table-fixed w-full text-center mt-16">
        <thead>
          <tr className="h-10 bg-blue-300 border-t border-black">
            <th className="w-1/6 text-center">Nomor</th>
            <th className="w-1/3">Mata Pelajaran</th>
            <th className="w-1/5">Aksi</th>
          </tr>
        </thead>
        <tbody>
          {subject ? subject.map((obj, position) => (
            <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-20">
              <td className="text-lg">{position + 1}</td>
              <td className="text-left font-semibold text-lg">{obj.name}</td>
              <td>
                <Link href={`attendance/${obj.id}`}>
                  <button className="mx-auto rounded md:text-base text-sm bg-yellow-400 px-4 py-2 text-white hover:bg-yellow-300 flex" type="button">
                    <PencilAltIcon className="hidden md:block h-5 w-5 mr-2" />
                    {' '}
                    Absensi Kehadiran
                  </button>
                </Link>
              </td>
            </tr>
          )) : (
            <tr>
              <td colSpan="3" className="text-2xl font-bold h-52">Memuat...</td>
            </tr>
          ) }
        </tbody>
      </table>
    </div>
  );
}
