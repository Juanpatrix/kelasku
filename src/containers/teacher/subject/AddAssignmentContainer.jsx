import { useState } from 'react';
import { Formik, Field } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

const validationSchema = Yup.object({
  name: Yup.string().required('Harap Isi Nama Tugas'),
  link: Yup.string().required('Harap Isi Link Tugas'),
});

const initialValues = {
  name: '',
  link: '',
};

export default function AddAssignmentContainer() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const { id } = router.query;

  const handleOnSubmit = async (values, {resetForm}) => {
    try {
      setLoading(true);
      const token = localStorage.getItem('user');
      values.subject_id = id;
      Class.PostAssignmentBySubject(token, values);
      setTimeout(() => {
        setLoading(false);
        router.back();
      }, 1000);
    } catch (error) {
      resetForm(initialValues);
    }
  };

  return (
    <div className="m-4">
      <div className="mt-10">
        <BackButton />
      </div>
      <div className="mt-8">
        <div>
          <h1 className="font-bold text-3xl">Buat Tugas</h1>
        </div>
        <div>
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            validationSchema={validationSchema}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
            }) => (
              <form className="mt-10" onSubmit={handleSubmit}>
                <div className="my-8">
                  <label htmlFor="name" className="md:flex items-center">
                    <h5 className="md:w-2/12 text-lg font-semibold my-3">Nama Tugas:</h5>
                    <Field
                      className="w-full border-2 border-blue-900 p-2 rounded"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      name="name"
                      id="name"
                      type="text"
                      placeholder="Topik"
                      required
                    />
                  </label>
                  <div className={`text-red-400 ${touched.title && errors.title ? 'error' : ''}`}>
                    {touched.title && errors.title && errors.title}
                  </div>
                </div>
                <div className="my-3 mb-20 ">
                  <label htmlFor="link" className="md:flex items-center">
                    <h5 className="md:w-2/12 text-lg font-semibold my-3">Link Tugas:</h5>
                    <Field
                      id="link"
                      type="url"
                      name="link"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="URL Tugas"
                      className="w-full border-2 border-blue-900 p-2 rounded"
                      required
                    />
                  </label>
                  <div className={`text-red-400 ${touched.content && errors.content ? 'error' : ''}`}>
                    {touched.content && errors.content && errors.content}
                  </div>
                </div>
                <div className="text-center">
                  <button className="bg-yellow-400 px-4 py-2 w-1/3 text-white hover:bg-yellow-300" type="submit" disabled={loading}>{loading ? 'Sedang Diproses...' : 'Submit'}</button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
