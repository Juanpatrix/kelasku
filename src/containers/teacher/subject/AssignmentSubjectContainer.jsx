/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { EyeIcon, PlusIcon, PencilAltIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

export default function AssignmentSubjectContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadAssignment = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemSubject = await Class.fetchAssignmentBySubject(token, id);
        setValue(itemSubject);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadAssignment();
  }, [router.isReady]);
  return (
    <div className="m-2">
      <div className="w-full justify-between mt-12 flex">
        <BackButton />
        <Link
          href={{
            pathname: '/teacher/subject/[id]/assignment/post',
            query: { id },
          }}
        >
          <a>
            <button type="button" className="bg-yellow-400 hover:bg-yellow-300 flex text-white px-5 mr-10 items-center text-sm font-semibold py-3 rounded">
              <PlusIcon className="h-5 w-5 mr-5" />
              Buat Tugas
            </button>
          </a>
        </Link>
      </div>
      <h2 className="text-center font-extrabold text-4xl">Tugas</h2>
      <table className="table-fixed mx-auto text-center w-full mt-16">
        <thead>
          <tr className="h-14 bg-blue-300 border-t border-black">
            <th className="w-1/12 text-lg text-center">No</th>
            <th className="w-1/6 text-lg text-center">Tanggal</th>
            <th className="w-1/2 ">Nama Tugas</th>
            <th className="w-1/4">Link Soal</th>
            <th className="w-1/4">Aksi</th>
          </tr>
        </thead>
        <tbody>
          {value
            ? value.length !== 0 ? value.map((obj, position) => (
              <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                <td>{position + 1}</td>
                <td>{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</td>
                <td className="text-left text-lg font-semibold">{obj.name}</td>
                <td className=" p-4">
                  <Link href={obj.link}>
                    <a target="_blank">
                      <button type="button" className="mx-auto rounded bg-yellow-400 hover:bg-yellow-300 text-white flex py-1 px-5 md:py-2 md:px-10 items-center">
                        <EyeIcon className="hidden md:block h-5 w-5 mr-2" />
                        {' '}
                        Lihat
                      </button>
                    </a>
                  </Link>
                </td>
                <td className=" p-4">
                  <Link href={{
                    pathname: '/teacher/subject/[id]/assignment/[id_asg]',
                    query: { id, id_asg: obj.id },
                  }}
                  >
                    <a>
                      <button type="button" className="mx-auto rounded bg-blue-900 hover:bg-blue-800 text-white flex py-1 px-5 md:py-2 md:px-10 items-center">
                        <PencilAltIcon className="hidden md:block h-5 w-5 mr-2" />
                        {' '}
                        Penilaian
                      </button>
                    </a>
                  </Link>
                </td>
              </tr>
            ))
              : (
                <tr>
                  <td colSpan="5" className="text-2xl font-bold h-52">Tugas belum tersedia</td>
                </tr>
              )
            : (
              <tr>
                <td colSpan="5" className="text-2xl font-bold h-52">Memuat...</td>
              </tr>
            )}
        </tbody>
      </table>
    </div>
  );
}
