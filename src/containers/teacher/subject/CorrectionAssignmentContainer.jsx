import { Formik, Field } from 'formik';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import * as Yup from 'yup';
import { DownloadIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

export default function ReadingSubjectContainer() {
  const [value, setValue] = useState([]);
  const [scores, setScore] = useState([]);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { id, idAsg } = router.query;

  const loadScore = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemAssignment = await Class.fetchAssignmentStudentBySubject(token, id, idAsg);
        const obj = {};
        setValue(itemAssignment);
        for (let i = 0; i < itemAssignment.length; i++) {
          obj[itemAssignment[i].id] = itemAssignment[i].score;
        }
        setScore(obj);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadScore();
  }, [router.isReady]);

  const handleOnSubmit = async (values, {resetForm}) => {
    try {
      const score = Object.values(values);
      const transform = score.map((obj, position) => ({
        id: value[position].id,
        score: obj,
      }));
      const token = localStorage.getItem('user');
      for (let i = 0; i < transform.length; i++) {
        Class.updateScore(token, transform[i]);
      }
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        router.push({
          pathname: '/teacher/subject/[id]/assignment',
          query: { id },
        });
      }, 1000);
    } catch (error) {
      // resetForm(initialValues);
    }
  };
  return (
    <div className="m-4">
      <div className="ml-10 mt-10">
        <BackButton />
      </div>
      <div className="mt-8">
        <h2 className="text-center font-extrabold text-2xl">
          {!value[0] ? '' : `Topik Tugas: ${value[0].assignment}`}
          {' '}
        </h2>
      </div>
      {!scores ? null
        : (
          <Formik
            initialValues={scores}
            onSubmit={handleOnSubmit}
            enableReinitialize
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
            }) => (
              <form onSubmit={handleSubmit}>
                <table className="table-fixed w-11/12 mx-auto text-center mt-16">
                  <thead>
                    <tr className="h-14 bg-blue-300 border-t border-black">
                      <th className="w-1/12 text-lg text-center">No</th>
                      <th className="w-1/3 text-lg">Nama</th>
                      <th className="w-1/3 text-lg">Link Tugas</th>
                      <th className="w-1/4 text-lg">Score</th>
                    </tr>
                  </thead>
                  <tbody>
                    {scores.length !== 0 & value.length !== 0 ? value.map((obj, position) => (
                      <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                        <td>{position + 1}</td>
                        <td className="text-left text-lg font-semibold">{obj.name}</td>
                        <td className="text-left text-lg font-semibold text-center">
                          {!obj.link
                            ? (
                              'Belum mengumpulkan tugas'
                            ) : (
                              <Link href={obj.link}>
                                <a target="_blank">
                                  <button type="button" className="p-2 rounded bg-yellow-400 hover:bg-yellow-300 text-white flex text-sm mx-auto items-center">
                                    <DownloadIcon className="h-5 w-5 mr-2" /> Akses Tugas
                                  </button>
                                </a>
                              </Link>
                            )}
                        </td>
                        <td className="text-center text-lg font-semibold">
                          <label htmlFor={obj.id}>
                            <Field
                              name={obj.id}
                              type="number"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className="w-1/2 p-2 rounded border-none focus:border-none outline-none focus:outline-none focus:ring-2 focus:ring-opacity-50 focus:ring-yellow-400"
                              placeholder="Nilai"
                            />
                          </label>
                          <div className={`text-red-400 ${touched.username && errors.username ? 'error' : ''}`}>
                            {touched.username && errors.username && errors.username}
                          </div>
                        </td>
                      </tr>
                    ))
                      : (
                        <tr>
                          <td colSpan="4" className="text-2xl font-bold h-52">Data Tugas Tidak Tersedia</td>
                        </tr>
                      )}
                  </tbody>
                </table>
                <div className="text-center mt-16">
                  <button className="bg-yellow-400 px-4 py-2 w-1/3 text-white hover:bg-yellow-300" type="submit" disabled={loading}>{loading ? 'Sedang Diproses...' : 'Submit'}</button>
                </div>
              </form>
            )}
          </Formik>
        )}
    </div>
  );
}
