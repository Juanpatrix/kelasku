import Link from 'next/link';
import Image from 'next/image';
import { useState, useEffect } from 'react';
import Class from '../../../libraries/class';
import { useRouter } from 'next/router';

export default function ListSubjectContainer() {
  const router = useRouter();
  const [value, setValue] = useState([]);
  const loadSubject = async () => {
    try {
      const token = localStorage.getItem('user');
      const subject = await Class.fetchSubject(token);
      setValue(subject);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadSubject();
  }, [router.isReady]);
  return (
    <div className="w-full text-center">
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8 m-8">
        {value.map((obj, position) => (
          <div key={position} className="rounded-2xl bg-gray-100 md:w-full sm:w-72 w-96 h-72 md:w-64 xl:h-96 xl:w-72 mx-auto filter drop-shadow-lg border-gray-300 border-2">
            <Link href={"subject/"+obj.id}>
              <a>
                <div className="h-3/4 block">
                  <div className="h-full relative">
                    <Image
                      alt="Kimia"
                      src={obj.image_url}
                      layout="fill"
                      objectFit="cover"
                    />
                  </div>
                </div>
                <div className="h-1/4 p-2 text-left block">
                  <h3 className="font-semibold text-2xl">{obj.name}</h3>
                  <h4 className="font-normal">{obj.description}</h4>
                </div>
              </a>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}
