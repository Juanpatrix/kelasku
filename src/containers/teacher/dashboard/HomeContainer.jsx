import Image from 'next/image';
import { ClipboardListIcon, BookmarkIcon } from '@heroicons/react/solid';
import { BookOpenIcon, PlusCircleIcon } from '@heroicons/react/outline';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import Calendar from 'react-calendar';
import Dashboard from '../../../libraries/dashboard';
import User from '../../../libraries/user';
import 'react-calendar/dist/Calendar.css';

export default function HomeContainer() {
  const [agenda, setAgenda] = useState([]);
  const [reading, setReading] = useState([]);
  const [user, setUser] = useState([{username: ''}]);
  const [assignment, setAssignment] = useState([]);
  const [value, setValue] = useState(new Date());
  const loadData = async () => {
    try {
      const token = localStorage.getItem('user');
      const itemSubject = await Dashboard.fetchReadingLatest(token);
      const userTemp = await User.fetchUser(token);
      const itemAssigment = await Dashboard.fetchSubmitLatest(token);
      setUser(userTemp);
      setAssignment(itemAssigment);
      const itemAgenda = await Dashboard.fetchAgendaTeacherLatest(token);
      setAgenda(itemAgenda);
      setReading(itemSubject);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadData();
  }, []);
  return (
    <div className="m-4">
      <div className="text-3xl font-extrabold mb-10">
        <h3>{`Selamat datang, ${user[0].username}`}</h3>
      </div>
      <div className="grid grid-cols-3 gap-10">
        {/* item 1 */}
        <div className="col-span-3 md:col-span-2">
          <div className="rounded-3xl bg-yellow-400 filter drop-shadow-xl flex p-6">
            <div className="text-white w-2/3 my-auto">
              <h3 className="text-lg md:text-2xl font-medium">Orang hebat bisa melahirkan beberapa karya bermutu tetapi guru yang bermutu dapat melahirkan ribuan orang-orang hebat</h3>
            </div>
            <div className="w-1/2 text-center">
              <Image src="/images/dashboard/teacher.png" width={200} height={200} />

            </div>
          </div>
        </div>
        {/* item 1 */}

        {/* item 2 */}
        <div className="bg-green-200 overflow-hidden row-span-2 rounded-lg filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold text-center mb-10">Kalender</h4>
          <Calendar
            onChange={setValue}
            value={value}
            className="mx-auto"
          />
        </div>
        {/* item 2 */}

        {/* item 3 */}
        <div className="bg-yellow-200 rounded-lg col-span-2 row-span-2 filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold">Siswa tercepat mengumpulkan tugas</h4>
          {/* Item Tugas */}
          <div className="grid grid-cols-3">
            { assignment.length === 0 ? (
              <div className="my-5 text-lg col-span-3 text-center font-semibold">
                Tidak ada siswa upload tugas
              </div>
            ) : (
              assignment.map((obj, position) => (
                <div key={position} className="bg-white m-4 p-3 rounded filter drop-shadow">
                  <p className="md:text-xl font-medium">{obj.name}</p>
                  <p className="md:text-lg flex items-center  my-2">
                    <BookOpenIcon className="h-5 w-5 mr-2" />
                    {' '}
                    {obj.mapel}
                  </p>
                  <p className="md:text-lg flex items-center my-2">
                    <ClipboardListIcon className="h-5 w-5 mr-2" />
                    {' '}
                    {obj.topic}
                  </p>
                  <div className="text-right mt-6">
                    <Link href={obj.jawab}>
                      <a target="_blank">
                        <button type="button" className="px-5 py-1 bg-blue-900 hover:bg-blue-800 text-white rounded">
                          Cek Tugas
                        </button>
                      </a>
                    </Link>
                  </div>
                </div>
              )))}

          </div>
          {/* Item Tugas */}
        </div>
        {/* item 3 */}

        {/* item 4 */}
        <div className="bg-purple-200 rounded-lg row-span-2 col-span-1 filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold">Agenda Terbaru</h4>
          {/* Item Agenda */}
          <div className="gap-5">
            {agenda.length === 0 ? (
              <div className="my-5 text-lg col-span-3 text-center font-semibold">
                Tidak ada agenda
              </div>
            ) : agenda.map((obj, position) => (
              <div key={position} className="bg-white m-4 p-3 rounded filter drop-shadow">
                <Link href={`teacher/agenda/detail/${obj.id}`}>
                  <a target="_blank">
                    <div className="flex hover:opacity-75 items-center my-2">
                      <div className="bg-blue-300 rounded-full mr-3">
                        <BookmarkIcon className="text-white h-5 w-5 m-3" />
                      </div>
                      {' '}
                      <div>
                        <p className="md:text-xl font-medium">{obj.title}</p>
                        <p className="md:text-lg">{obj.tags}</p>
                      </div>
                    </div>
                  </a>
                </Link>
              </div>
            ))}
          </div>
          {/* Item Agenda */}
        </div>
        {/* item 4 */}

        {/* item 5 */}
        <div className="bg-blue-200 col-span-2 row-span-2 rounded-lg filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold">Materi Terbaru</h4>
          {/* Item Materi */}
          <div className="grid grid-cols-3">
            {reading.map((obj, position) => (
              <div key={position} className="bg-white my-4 col-span03 mx-6 p-3 rounded filter drop-shadow">
                <div className="flex items-center my-2">
                  <div className="mr-3">
                    <Image src={obj.pic} width={50} height={50} className="rounded-xl" />
                  </div>
                  {' '}
                  <div>
                    <p className="md:text-xl font-semibold">{obj.mapel}</p>
                    <p className="md:text-lg">{obj.name}</p>
                  </div>
                </div>
                <div className="text-right mt-6">
                  <Link href={obj.link}>
                    <a target="_blank">
                      <button type="button" className="px-5 py-1 bg-blue-900 hover:bg-blue-800 text-white rounded">
                        Lihat
                      </button>
                    </a>
                  </Link>
                </div>
              </div>
            ))}
          </div>
          {/* Item Materi */}
        </div>
        {/* item 5 */}

        {/* item 6 */}
        <div>
          <Link href="teacher/agenda/post">
            <a>
              <div className="rounded h-72 border-4 hover:bg-yellow-500 hover:text-white border-black border-dashed flex flex-col items-center justify-center bg-white">
                <PlusCircleIcon className="w-20 h-20" />
                <p className="text-xl mt-5 font-semibold">Tambah Agenda</p>
              </div>
            </a>
          </Link>
        </div>
        {/* item 6 */}
      </div>
    </div>
  );
}
