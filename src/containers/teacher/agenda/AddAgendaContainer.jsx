import { MultiSelect } from 'react-multi-select-component';
import { useState, useEffect } from 'react';
import { Formik, Field } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import BackButton from '@/components/BackButton';
import Agenda from '../../../libraries/agenda';

const categories = [
  { name: 'Informasi' },
  { name: 'Pembayaran' },
  { name: 'Perilaku' },
  { name: 'Tugas' },
  { name: 'Ujian' },
  { name: 'Lainnya' },
];

const validationSchema = Yup.object({
  title: Yup.string().required('Harap Isi Judul'),
  content: Yup.string().required('Harap Isi Deskripsi Agenda'),
});

const initialValues = {
  title: '',
  content: '',
  tags: 'Informasi',
};

export default function AddAgendaContainer() {
  const router = useRouter();
  const [member, setMember] = useState([]);
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState([]);
  useEffect(async () => {
    if (!router.isReady) return;
    const token = localStorage.getItem('user');
    const student = await Agenda.fetchStudent(token);
    setValue(student);
  }, [router.isReady]);

  const members = value.map((obj) => (
    {
      value: obj.id,
      label: obj.name,
    }
  ));

  const handleOnSubmit = async (values, {resetForm}) => {
    try {
      setLoading(true);
      const temp = [];
      for (let i = 0; i < member.length; i++) {
        for (let j = 0; j < value.length; j++) {
          if (member[i].value === value[j].id) {
            temp.push(value[j].parent_id);
          }
        }
      }
      const transform = member.map((obj, position) => ({
        title: values.title,
        parent_id: temp[position],
        student_id: obj.value,
        content: values.content,
        tags: values.tags,
      }));
      const token = localStorage.getItem('user');
      for (let i = 0; i < transform.length; i++) {
        Agenda.postAgenda(token, transform[i]);
      }
      setTimeout(() => {
        setLoading(false);
        router.replace('../agenda');
      }, 1000);
    } catch (error) {
      resetForm(initialValues);
    }
  };

  return (
    <div className="m-4">
      <div className="mt-8">
        <BackButton />
      </div>
      <div className="mt-8 mr-10">
        <div>
          <h1 className="font-bold text-3xl">Buat Buku Agenda</h1>
        </div>
        <div>
          <Formik
            initialValues={initialValues}
            onSubmit={handleOnSubmit}
            validationSchema={validationSchema}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              touched,
              errors,
            }) => (
              <form className="mt-10" onSubmit={handleSubmit}>
                <div className="my-3">
                  <label className="md:flex items-center my-8">
                    <h5 className="w-2/12 text-lg font-semibold">Kepada:</h5>
                    <MultiSelect
                      className="border-blue-900 border-2 rounded w-full"
                      options={members}
                      value={member}
                      onChange={setMember}
                      labelledBy="Select"
                    />
                  </label>
                </div>
                <div className="my-8">
                  <label htmlFor="title" className="md:flex items-center">
                    <h5 className="w-2/12 text-lg font-semibold">Judul:</h5>
                    <Field
                      className="w-full border-2 border-blue-900 p-2 rounded"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      name="title"
                      id="title"
                      type="text"
                      placeholder="Judul Agenda"
                      required
                    />
                  </label>
                  <div className={`text-red-400 ${touched.title && errors.title ? 'error' : ''}`}>
                    {touched.title && errors.title && errors.title}
                  </div>
                </div>
                <div className="my-3">
                  <label htmlFor="tags" className="md:flex items-center my-8">
                    <h5 className="w-2/12 text-lg font-semibold">Kategori:</h5>
                    <Field
                      name="tags"
                      id="tags"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className="w-full border-2 border-blue-900 p-2 rounded"
                      as="select"
                      required
                    >
                      {categories.map((obj) => (
                        <option key={obj.name} value={obj.name}>{obj.name}</option>
                      ))}
                    </Field>
                  </label>
                </div>
                <div className="my-3 mb-10 ">
                  <label htmlFor="content" className="my-8">
                    <h5 className="w-1/12 text-lg font-semibold my-5">Deskripsi:</h5>
                    <Field
                      as="textarea"
                      id="content"
                      name="content"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Deskripsi Agenda"
                      className="p-2 w-full h-40 border-2 rounded border-blue-900"
                      required
                    />
                  </label>
                  <div className={`text-red-400 ${touched.content && errors.content ? 'error' : ''}`}>
                    {touched.content && errors.content && errors.content}
                  </div>
                </div>
                <div className="text-center">
                  <button className="bg-yellow-400 px-4 py-2 w-1/3 text-white hover:bg-yellow-300" type="submit" disabled={loading}>{loading ? 'Sedang Diproses...' : 'Submit'}</button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
