import {ChevronLeftIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import agenda from '../../../libraries/agenda';

export default function DetailAgendaContainer() {
  const [value, setValue] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const loadAgenda = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemAgenda = await agenda.fetchAgenda(token, id);
        setValue(itemAgenda);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadAgenda();
  }, [router.isReady]);
  return (
    <div className="m-4 p-2">
      <div className="mt-8">
        <button className="bg-blue-900 hover:bg-blue-800 text-white px-6 py-2 rounded" onClick={()=> window.close()}>Tutup Agenda</button>
      </div>
      <div className="md:m-16 mt-20 border-gray-200 border-2 rounded">
        <div className="m-8">
          <h1 className="mb-4 font-bold text-3xl">
            {value.title}
          </h1>
          <p className="my-2 text-lg font-semibold">
            Kategori:
            {' '}
            {value.tags}
          </p>
          <p className="font-medium">
            {value.content}
          </p>
        </div>
      </div>
    </div>
  );
}
