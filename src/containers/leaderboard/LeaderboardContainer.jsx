/* eslint-disable no-nested-ternary */
import Image from 'next/image';
import { Fragment, useState, useEffect } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';
import Leaderboard from '../../libraries/leaderboard';

const subjects = [
  { name: 'Semua Mata Pelajaran', id: '' },
  { name: 'Ppkn', id: '1' },
  { name: 'Bahasa Indonesia', id: '2' },
  { name: 'IPA', id: '3' },
  { name: 'Seni Budaya', id: '4' },
  { name: 'Matematika', id: '5' },
  { name: 'Bahasa Inggris', id: '6' },
  { name: 'IPS', id: '7' },
  { name: 'Olahraga', id: '8' },
];

export default function LeaderboardContainer() {
  const [selected, setSelected] = useState(subjects[0]);
  const [score, setScore] = useState();
  const [rejectScore, setRejectScore] = useState();
  const loadScore = async () => {
    try {
      const token = localStorage.getItem('user');
      const scoreStudent = await Leaderboard(token, selected.id);
      const nullScore = scoreStudent.filter((obj) => !obj.totalscore);
      const scoreValid = scoreStudent.filter((obj) => obj.totalscore);
      setScore(scoreValid);
      setRejectScore(nullScore);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadScore();
  }, [selected.id]);
  return (
    <div className="m-4">
      <div className="flex m-10 mb-6">
        <div className="bg-blue-900 rounded md:w-1/3 sm:w-1/2">
          <Listbox value={selected} onChange={setSelected}>
            <div className="relative mt-1">
              <Listbox.Button className="relative flex w-full text-left shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white sm:text-sm">
                <span className="block py-2 px-4 w-full text-white">{selected.name}</span>
                <span className="py-2 px-4 pointer-events-none">
                  <SelectorIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                </span>
              </Listbox.Button>
              <Transition
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options className="z-10 absolute w-full py-1 mt-1 overflow-auto bg-white rounded-md shadow-lg max-h-80 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                  {subjects.map((subject, subjectIdx) => (
                    <Listbox.Option
                      key={subjectIdx}
                      className="cursor-default select-none relative py-2 pl-10 pr-4"
                      value={subject}
                    >
                      {({ selected }) => (
                        <>
                          <span
                            className={`${
                              selected ? 'font-medium' : 'font-normal'
                            } block truncate`}
                          >
                            {subject.name}
                          </span>
                          {selected ? (
                            <span
                              className="text-amber-600 absolute inset-y-0 left-0 flex items-center pl-3"
                            >
                              <CheckIcon className="w-5 h-5" aria-hidden="true" />
                            </span>
                          ) : null}
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </Listbox>
        </div>
      </div>
      <table className="table-fixed w-11/12 text-center m-10">
        <thead>
          <tr className="h-16 text-xl font-semibold bg-blue-300 border-t border-black">
            <th className="w-1/5 text-center">Peringkat</th>
            <th className="w-1/2 ">Nama</th>
            <th className="w-1/4">Nilai</th>
          </tr>
        </thead>
        <tbody>
          {score
            ? (
              <>
                {score.length !== 0 ? score.map((obj, position) => (position === 0 ? (
                  <tr key={position} className="border-2 text-lg font-semibold bg-blue-100 border-gray-300 h-16">
                    <td><Image width={70} height={70} src="/images/medal/First-Badge.png" className="h-100 mx-auto" alt="1" /></td>
                    <td className="text-left">{obj.name}</td>
                    <td>{obj.totalscore}</td>
                  </tr>
                ) : position === 1 ? (
                  <tr key={position} className="border-2 text-lg font-semibold bg-blue-100 border-gray-300 h-16">
                    <td><Image width={70} height={70} src="/images/medal/Silver-Badge.png" className="h-100 mx-auto" alt="2" /></td>
                    <td className="text-left">{obj.name}</td>
                    <td>{obj.totalscore}</td>
                  </tr>
                ) : position === 2 ? (
                  <tr key={position} className="border-2 text-lg font-semibold bg-blue-100 border-gray-300 h-16">
                    <td><Image width={70} height={70} src="/images/medal/Bronze-Badge.png" className="h-100 mx-auto" alt="3" /></td>
                    <td className="text-left">{obj.name}</td>
                    <td>{obj.totalscore}</td>
                  </tr>
                ) : (
                  <tr key={position} className="border-2 text-lg font-semibold bg-blue-100 border-gray-300 h-16">
                    <td>{position + 1}</td>
                    <td className="text-left">{obj.name}</td>
                    <td>{obj.totalscore}</td>
                  </tr>
                ))) : (
                  <tr />
                )}
                {rejectScore
                  ? (rejectScore.length !== 0 || score.length > 0 ? rejectScore.map((obj, position) => (
                    <tr key={position} className="border-2 text-lg font-semibold bg-blue-100 border-gray-300 h-16">
                      <td>Tidak Diketahui</td>
                      <td className="text-left">{obj.name}</td>
                      <td>Nilai belum dapat dilihat</td>
                    </tr>
                  )) : (
                    <tr>
                      <td colSpan="3" className="text-2xl font-semibold h-52">Data Nilai belum tersedia karena belum ada tugas</td>
                    </tr>
                  )) : (
                    <tr />
                  )}
              </>
            )
            : (
              <tr>
                <td colSpan="3" className="text-2xl font-bold h-52">Menampilkan...</td>
              </tr>
            )}
        </tbody>
      </table>
    </div>
  );
}
