/* eslint-disable no-nested-ternary */
import { Formik, Field } from 'formik';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import * as Yup from 'yup';
import { EyeIcon, DownloadIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

const validationSchema = Yup.object({
  link: Yup.string().required('Harap Isi Link Tugas'),
});

const initialValues = {
  link: '',
};

export default function SubmitAssignmentContainer() {
  const [value, setValue] = useState(false);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { id, idAsg } = router.query;

  const handleOnSubmit = async (values, {resetForm}) => {
    try {
      setLoading(true);
      const token = localStorage.getItem('user');
      Class.updateAnswer(token, values, idAsg);
      setTimeout(() => {
        setLoading(false);
        router.push({
          pathname: '/student/subject/[id]/assignment',
          query: { id },
        });
      }, 1000);
    } catch (error) {
      resetForm(initialValues);
    }
  };

  const loadAssignment = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemAssignment = await Class.fetchStatusAssignment(token, id, idAsg);
        setValue(itemAssignment.reduce((obj, item) => (
          {
            ...obj,
            id: item.id,
            score: item.score,
            soal: item.soal,
            topic: item.topic,
            link: item.link,
          }), {}));
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadAssignment();
  }, [router.isReady]);

  return (
    <div className="m-4">
      <div className="mt-10 ml-4">
        <BackButton />
      </div>
      {!value ? (
        <div>
          Menunggu...
        </div>
      ) : !value.score ? (
        <div className="mt-8 ml-4">
          <div>
            <h1 className="font-bold text-3xl">{!value.topic ? 'Tidak ada topik' : value.topic}</h1>
          </div>
          <div>
            <table className="table-fixed w-1/2 mt-8">
              <tbody>
                <tr className="h-12">
                  <td>Link Tugas</td>
                  <td>
                    {!value.soal ? 'Tidak Diketahui'
                      : (
                        <Link href={value.soal}>
                          <a target="_blank">
                              <button type="button" className="mx-auto rounded bg-blue-900 hover:bg-blue-800 text-white flex py-1 px-5 md:px-8 items-center">
                                <EyeIcon className="h-5 w-5 mr-2" />
                                {' '}
                                <p className="w-20">Lihat Soal</p>
                              </button>
                            </a>
                        </Link>
                      )}
                  </td>
                </tr>
                <tr className="h-12">
                  <td>Terkumpul</td>
                  <td className="text-center">
                    {!value.link ? <p className="text-center">Belum Ada</p> : (
                      <Link href={value.link}>
                        <a target="_blank">
                          <button type="button" className="mx-auto rounded bg-blue-900 hover:bg-blue-800 text-white flex py-1 px-2 md:px-5 items-center">
                              <DownloadIcon className="h-5 w-5 mr-2" />
                              {' '}
                              <p className="w-28">Buka Jawaban</p>
                            </button>
                        </a>
                      </Link>
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
            <div>
              <div className="my-6">
                <h2 className="font-bold text-2xl">Submit Tugas</h2>
              </div>
              {!value.id ? 'Tidak Tersedia'
                : (
                  <Formik
                    initialValues={initialValues}
                    onSubmit={handleOnSubmit}
                    validationSchema={validationSchema}
                  >
                    {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      touched,
                      errors,
                    }) => (
                      <form onSubmit={handleSubmit}>
                        <div className="mb-16">
                            <label htmlFor="link" className="md:flex items-center">
                              <h5 className="md:w-2/12 text-lg font-semibold my-3">Link URL:</h5>
                              <Field
                                id="link"
                                type="url"
                                name="link"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder="URL Materi"
                                className="w-full border-2 border-blue-900 p-2 rounded mr-10"
                                required
                              />
                            </label>
                          </div>
                        <div className="text-center">
                            <button className="bg-yellow-400 px-4 py-2 w-1/3 text-white hover:bg-yellow-300" type="submit" disabled={loading}>{loading ? 'Sedang Diproses...' : 'Submit'}</button>
                          </div>
                      </form>
                    )}
                  </Formik>
                )}
            </div>
          </div>
        </div>
      )
        : (
          <div className="mt-52 w-2/5 text-center mx-auto">
            <h4 className="font-semibold text-2xl mb-10">Maaf tidak dapat submit ulang tugas yang sudah dikoreksi. Harap menghubungi guru terkait</h4>
          </div>
        )}
    </div>
  );
}
