/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import {
  XIcon, DownloadIcon, UploadIcon,
} from '@heroicons/react/solid';
import Link from 'next/link';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

export default function AssignmentSubjectContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id, idAsg } = router.query;
  const loadSubject = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemSubject = await Class.fetchAssignmentStudent(token, id);
        setValue(itemSubject);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadSubject();
  }, [router.isReady]);
  return (
    <div className="m-2">
      <div className="ml-10 mt-10">
        <BackButton />
      </div>
      <h2 className="text-center font-extrabold text-4xl mt-8">
        Tugas
      </h2>
      <table className="table-fixed w-5/6 mx-auto text-center mt-16">
        <thead>
          <tr className="h-14 bg-blue-300 border-t border-black">
            <th className="w-1/12 text-lg text-center">No</th>
            <th className="w-1/6 text-lg text-center">Tanggal</th>
            <th className="w-1/2">Nama Tugas</th>
            <th className="w-1/3">Jawaban Terkumpul</th>
            <th className="w-1/3">Score</th>
          </tr>
        </thead>
        <tbody>
          {value
            ? value.length !== 0 ? value.map((obj, position) => (
              <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                <td>{position + 1}</td>
                <td>{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</td>
                <td className="text-left text-lg font-semibold">{obj.topic}</td>
                <td className="text-left text-lg font-semibold">
                  {!obj.link
                    ? (
                      <XIcon className="text-red-800 mx-auto w-8" />
                    )
                    : (
                      <Link href={obj.link}>
                        <a target="_blank">
                          <button type="button" className="mx-auto rounded bg-yellow-400 hover:bg-yellow-300 text-sm text-white flex py-2 px-2 items-center">
                            <DownloadIcon className="h-5 w-5 mr-2" />
                            {' '}
                            Akses Jawaban
                        </button>
                        </a>
                      </Link>
                    )}
                </td>
                <td className="text-center text-lg font-semibold">
                  {!obj.score
                    ? (
                      <Link
                        href={{
                          pathname: '/student/subject/[id]/assignment/[id_asg]',
                          query: { id, id_asg: obj.id },
                        }}
                      >
                        <a>
                          <button type="button" className="mx-auto rounded bg-blue-900 hover:bg-blue-800 text-sm text-white flex py-2 px-2 items-center">
                            <UploadIcon className="h-5 w-5 mr-2" />
                            {' '}
                            Upload Tugas
                        </button>
                        </a>
                      </Link>
                    ) : obj.score }
                </td>
              </tr>
            ))
              : (
                <tr>
                  <td colSpan="5" className="text-2xl font-bold h-52">Tidak Ada Tugas</td>
                </tr>
              )
            : (
              <tr>
                <td colSpan="5" className="text-2xl font-bold h-52">Memuat...</td>
              </tr>
            )}
        </tbody>
      </table>
    </div>
  );
}
