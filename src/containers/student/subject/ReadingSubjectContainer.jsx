/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { EyeIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

export default function ReadingSubjectContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadSubject = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemSubject = await Class.fetchReadingBySubject(token, id);
        setValue(itemSubject);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadSubject();
  }, [router.isReady]);
  return (
    <div className="m-4">
      <div className="ml-10 mt-10">
        <BackButton />
      </div>
      <div className="mt-8">
        <h2 className="text-center font-extrabold text-4xl">Materi Bacaan </h2>
      </div>
      <table className="table-fixed w-11/12 mx-auto text-center mt-16">
        <thead>
          <tr className="h-14 bg-blue-300 border-t border-black">
            <th className="w-1/12 text-lg text-center">No</th>
            <th className="w-1/6 text-lg text-center">Tanggal</th>
            <th className="w-1/3 text-lg">Topik</th>
            <th className="w-1/4 text-lg">Aksi</th>
          </tr>
        </thead>
        <tbody>
          {value
            ? value.length !== 0 ? value.map((obj, position) => (
              <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                <td>{position + 1}</td>
                <td>{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</td>
                <td className="text-left text-lg font-semibold">{obj.name}</td>
                <td className=" p-4">
                  <Link href={obj.link}>
                    <a target="_blank">
                      <button type="button" className="mx-auto rounded bg-yellow-400 hover:bg-yellow-300 text-white flex py-1 px-5 md:py-2 md:px-10 items-center">
                        <EyeIcon className="h-5 w-5 mr-2" />
                        {' '}
                        Lihat
                      </button>
                    </a>
                  </Link>
                </td>
              </tr>
            ))
              : (
                <tr>
                  <td colSpan="4" className="text-2xl font-bold h-52">Materi belum tersedia</td>
                </tr>
              )
            : (
              <tr>
                <td colSpan="4" className="text-2xl font-bold h-52">Memuat...</td>
              </tr>
            )}
        </tbody>
      </table>
    </div>
  );
}
