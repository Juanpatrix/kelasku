import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

export default function DetailSubjectContainer() {
  const [value, setValue] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const loadSubject = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemSubject = await Class.fetchSubjectById(token, id);
        setValue(itemSubject);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadSubject();
  }, [router.isReady]);
  return (
    <div className="m-4">
      <div className="ml-10 mt-10">
        <BackButton />
      </div>
      <div className="mt-8">
        <h2 className="text-center font-extrabold text-4xl">{value.name}</h2>
      </div>
      {value.name ? (
        <div className="md:flex m-10 gap-32 items-center justify-center">
          <Link href={`${value.id}/reading`}>
            <a>
              <div className="text-center">
                <Image src="/images/subject/Notebook.png" width={400} height={400} className="" alt="materi bacaan" />
              </div>
            </a>
          </Link>
          <Link href={`${value.id}/assignment`}>
            <a>
              <div className="text-center md:mt-0 mt-14">
                <Image src="/images/subject/Assignment.png" width={400} height={400} className="" alt="tugas" />
              </div>
            </a>
          </Link>
        </div>
      ) : (
        <div className="mt-10 font-bold text-2xl text-center">
          Memuat...
        </div>
      )}
    </div>
  );
}
