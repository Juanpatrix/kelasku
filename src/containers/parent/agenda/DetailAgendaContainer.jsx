import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import agenda from '../../../libraries/agenda';

export default function DetailAgendaContainer() {
  const [value, setValue] = useState([]);
  const router = useRouter();
  const { id } = router.query;
  const loadAgenda = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemAgenda = await agenda.fetchAgendaParent(token, id);
        setValue(itemAgenda);
      }
    } catch (error) {
      router.push('../agenda');
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadAgenda();
  }, [router.isReady]);
  return (
    <div className="m-4 p-2">
      <div className="mt-8">
        <button className="bg-blue-900 hover:bg-blue-800 text-white px-6 py-2 rounded" onClick={() => window.close()}>Tutup Agenda</button>
      </div>
      <div className="md:m-16 mt-20 border-gray-200 border-2 rounded">
        <div className="m-8">
          <h1 className="mb-4 font-bold text-3xl">
            {!value[0] ? 'Topik Tidak Ditemukan' : value[0].title }
          </h1>
          <p className="my-2 text-lg font-semibold">
            {!value[0] ? '' : `Kategori: ${value[0].tags}` }
          </p>
          <p className="font-medium">
            {!value[0] ? '' : value[0].tags }
          </p>
        </div>
      </div>
    </div>
  );
}
