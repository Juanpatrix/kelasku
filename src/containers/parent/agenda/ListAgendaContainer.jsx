/* eslint-disable no-nested-ternary */
import {
  CheckIcon,
  SelectorIcon,
  PlusIcon,
  TagIcon,
  PencilIcon,
  CashIcon,
  ClipboardListIcon,
  AnnotationIcon,
  InformationCircleIcon,
  EmojiHappyIcon,
} from '@heroicons/react/solid';
import { CalendarIcon } from '@heroicons/react/outline';
import { Fragment, useState, useEffect } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import Link from 'next/link';
import agenda from '../../../libraries/agenda';

const categories = [
  { name: 'Informasi' },
  { name: 'Pembayaran' },
  { name: 'Perilaku' },
  { name: 'Tugas' },
  { name: 'Ujian' },
  { name: 'Lainnya' },
];

export default function ListAgendaContainer() {
  const [selected, setSelected] = useState(categories[0]);
  const [value, setValue] = useState([]);

  const loadAgenda = async () => {
    try {
      const token = localStorage.getItem('user');
      const agendas = await agenda.fetchAgendaByParent(token);
      setValue(agendas);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadAgenda();
  }, []);
  return (
    <div>
      <div className="m-10 mb-6 md:flex">
        <div className="bg-blue-900 rounded md:w-1/3 sm:w-1/2">
          <Listbox value={selected} onChange={setSelected}>
            <div className="relative mt-1">
              <Listbox.Button className="relative flex w-full text-left shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white sm:text-sm">
                <span className="block py-2 px-4 w-full text-white">{selected.name}</span>
                <span className="py-2 px-4 pointer-events-none">
                  <SelectorIcon
                    className="w-5 h-5 text-gray-400"
                    aria-hidden="true"
                  />
                </span>
              </Listbox.Button>
              <Transition
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto bg-white rounded-md shadow-lg max-h-80 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                  {categories.map((category, categoryIdx) => (
                    <Listbox.Option
                      key={categoryIdx}
                      className="cursor-default select-none relative py-2 pl-10 pr-4"
                      value={category}
                    >
                      {({ selected}) => (
                        <>
                          <span
                            className={`${
                              selected ? 'font-medium' : 'font-normal'
                            } block truncate`}
                          >
                            {category.name}
                          </span>
                          {selected ? (
                            <span
                              className="text-amber-600 absolute inset-y-0 left-0 flex items-center pl-3"
                            >
                              <CheckIcon className="w-5 h-5" aria-hidden="true" />
                            </span>
                          ) : null}
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </Listbox>
        </div>
      </div>

      {
        value.map((obj) => (
          obj.tags === 'Informasi' && obj.tags === selected.name
            ? (
              <Link href={`agenda/detail/${obj.id}`}>
                <a target="_blank">
                  <div className="mx-8 flex my-4 flex-row bg-blue-200 p-8 items-center">
                    <div className="mr-16 md:flex hidden w-40 h-40 bg-blue-400 text-blue-300">
                      <InformationCircleIcon className="w-32 h-32 m-auto" />
                    </div>
                    <div className="w-full md:w-7/12">
                      <h1 className="md:text-3xl pb-4 text-2xl">
                        {obj.title}
                        {' '}
                      </h1>
                      <div className="flex items-center">
                        <span><CalendarIcon className="my-2 text-blue-400 w-5 h-5 mr-2" /></span>
                        <span className="text-sm md:text-base">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</span>
                        {' '}

                      </div>
                      <div className="flex items-center">
                        <span><TagIcon className="text-blue-400 w-5 h-5 mr-2" /></span>
                        <span className="text-sm md:text-base">Informasi</span>
                        {' '}

                      </div>
                      <p className="md:block hidden w-96 truncate mt-2">{obj.content}</p>
                    </div>
                  </div>
                </a>
              </Link>
            )
            : obj.tags === 'Pembayaran' && obj.tags === selected.name
              ? (
                <Link href={`agenda/detail/${obj.id}`}>
                  <a target="_blank">
                    <div className="mx-8 flex my-4 flex-row bg-green-200 p-8 items-center">
                      <div className="mr-16 hidden md:flex w-40 h-40 bg-green-400 text-green-300">
                        <CashIcon className="w-32 h-32 m-auto" />
                      </div>
                      <div className="w-full md:w-7/12">
                        <h1 className="md:text-3xl pb-4 text-2xl">{obj.title}</h1>
                        <div className="flex items-center">
                          <span><CalendarIcon className="my-2 text-green-400 w-5 h-5 mr-2" /></span>
                          <span className="text-sm md:text-base">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</span>
                          {' '}

                        </div>
                        <div className="flex items-center">
                          <span><TagIcon className="text-green-400 w-5 h-5 mr-2" /></span>
                          <span className="text-sm md:text-base">Pembayaran</span>
                          {' '}

                        </div>
                        <p className="md:block hidden w-96 truncate mt-2">{obj.content}</p>
                      </div>
                    </div>
                  </a>
                </Link>
              )
              : obj.tags === 'Perilaku' && obj.tags === selected.name
                ? (
                  <Link href={`agenda/detail/${obj.id}`}>
                    <a target="_blank">
                      <div className="mx-8 flex my-4 flex-row bg-gray-200 p-8 items-center">
                        <div className="mr-16 hidden md:flex w-40 h-40 bg-gray-400  text-gray-300">
                          <EmojiHappyIcon className="w-32 h-32 m-auto" />
                        </div>
                        <div className="w-full md:w-7/12">
                          <h1 className="md:text-3xl pb-4 text-2xl">{obj.title}</h1>
                          <div className="flex items-center">
                            <span><CalendarIcon className="my-2 text-gray-400 w-5 h-5 mr-2" /></span>
                            <span className="text-sm md:text-base">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</span>
                            {' '}

                          </div>
                          <div className="flex items-center">
                            <span><TagIcon className="text-gray-400 w-5 h-5 mr-2" /></span>
                            <span className="text-sm md:text-base">Perilaku</span>
                            {' '}

                          </div>
                          <p className="md:block hidden w-96 truncate mt-2">{obj.content}</p>
                        </div>
                      </div>
                    </a>
                  </Link>
                )
                : obj.tags === 'Tugas' && obj.tags === selected.name
                  ? (
                    <Link href={`agenda/detail/${obj.id}`}>
                      <a target="_blank">
                        <div className="mx-8 flex my-4 flex-row bg-yellow-200 p-8 items-center">
                          <div className="mr-16 hidden md:flex w-40 h-40 bg-yellow-400 text-yellow-300">
                            <PencilIcon className="w-32 h-32 m-auto" />
                          </div>
                          <div className="w-full md:w-7/12">
                            <h1 className="md:text-3xl pb-4 text-2xl">{obj.title}</h1>
                            <div className="flex items-center">
                              <span><CalendarIcon className="my-2 text-yellow-400 w-5 h-5 mr-2" /></span>
                              <span className="text-sm md:text-base">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</span>
                              {' '}

                            </div>
                            <div className="flex items-center">
                              <span><TagIcon className="text-yellow-400 w-5 h-5 mr-2" /></span>
                              <span className="text-sm md:text-base">Tugas</span>
                              {' '}

                            </div>
                            <p className="md:block hidden w-96 truncate mt-2">{obj.content}</p>
                          </div>
                        </div>
                      </a>
                    </Link>
                  )
                  : obj.tags === 'Ujian' && obj.tags === selected.name
                    ? (
                      <Link href={`agenda/detail/${obj.id}`}>
                        <a target="_blank">
                          <div className="mx-8 flex my-4 flex-row bg-pink-200 p-8 items-center">
                            <div className="mr-16 hidden md:flex w-40 h-40 bg-pink-400 text-pink-300">
                              <ClipboardListIcon className="w-32 h-32 m-auto" />
                            </div>
                            <div className="w-full md:w-7/12">
                              <h1 className="md:text-3xl pb-4 text-2xl">{obj.title}</h1>
                              <div className="flex items-center">
                                <span><CalendarIcon className="my-2 text-pink-400 w-5 h-5 mr-2" /></span>
                                <span className="text-sm md:text-base">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</span>
                                {' '}

                              </div>
                              <div className="flex items-center">
                                <span><TagIcon className="text-pink-400 w-5 h-5 mr-2" /></span>
                                <span className="text-sm md:text-base">Ujian</span>
                                {' '}

                              </div>
                              <p className="md:block hidden w-96 truncate mt-2">{obj.content}</p>
                            </div>
                          </div>
                        </a>
                      </Link>
                    )
                    : obj.tags === 'Lainnya' && obj.tags === selected.name
                      ? (
                        <Link href={`agenda/detail/${obj.id}`}>
                          <a target="_blank">
                            <div className="mx-8 flex my-4 flex-row bg-yellow-500 p-8 items-center">
                              <div className="mr-16 hidden md:flex w-40 h-40 bg-yellow-700  text-yellow-600">
                                <AnnotationIcon className="w-32 h-32 m-auto" />
                              </div>
                              <div className="w-full md:w-7/12">
                                <h1 className="md:text-3xl pb-4 text-2xl">{obj.title}</h1>
                                <div className="flex items-center">
                                  <span><CalendarIcon className="my-2 text-yellow-700 w-5 h-5 mr-2" /></span>
                                  <span className="text-sm md:text-base">{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</span>
                                  {' '}

                                </div>
                                <div className="flex items-center">
                                  <span><TagIcon className="text-yellow-700 w-5 h-5 mr-2" /></span>
                                  <span className="text-sm md:text-base">Lainnya</span>
                                  {' '}

                                </div>
                                <p className="md:block hidden w-96 truncate mt-2">{obj.content}</p>
                              </div>
                            </div>
                          </a>
                        </Link>
                      )
                      : (<div />)

        ))
      }
    </div>
  );
}
