/* eslint-disable no-nested-ternary */
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Attendance from '../../../libraries/attendance';

export default function AttendancePage() {
  const [list, setList] = useState(false);
  const [izin, setIzin] = useState(0);
  const [sakit, setSakit] = useState(0);
  const [alpha, setAlpha] = useState(0);
  const loadAttendance = async () => {
    try {
      const token = localStorage.getItem('user');
      const itemAttendance = await Attendance.fetchAttendanceParent(token);
      setList(itemAttendance);
      let statusI = 0;
      let statusS = 0;
      let statusA = 0;
      for (let i = 0; i < itemAttendance.length; i++) {
        statusI += Number(itemAttendance[i].izin);
        statusA += Number(itemAttendance[i].alpha);
        statusS += Number(itemAttendance[i].sakit);
      }
      setIzin(statusI);
      setAlpha(statusA);
      setSakit(statusS);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadAttendance();
  }, []);
  return (
    <div>
      <div className="grid gap-8 grid-cols-1 sm:w-5/6 md:gap-2  md:grid-cols-3 m-10 mb-6">
        <div className="bg-yellow-500 flex place-items-center text-white rounded-lg py-2 px-4">
          <div className="w-full ml-4">
            <h4 className="font-semibold text-2xl mb-2">Sakit</h4>
            <h6 className="font-extrabold text-5xl">{sakit}</h6>
          </div>
          <Image width={250} height={250} src="/images/parent/Sakit.png" alt="sakit" />
        </div>
        <div className="bg-blue-900 flex place-items-center text-white rounded-lg py-2 px-4">
          <div className="w-full ml-4">
            <h4 className="font-semibold text-2xl mb-2">Izin</h4>
            <h6 className="font-extrabold text-5xl">{izin}</h6>
          </div>
          <Image width={250} height={250} src="/images/parent/Izin.png" alt="izin" />
        </div>
        <div className="bg-red-400 flex place-items-center text-white rounded-lg py-2 px-4">
          <div className="w-full ml-4">
            <h4 className="font-semibold text-2xl mb-2">Alpha</h4>
            <h6 className="font-extrabold text-5xl">{alpha}</h6>
          </div>
          <Image width={250} height={250} src="/images/parent/Alpha.png" alt="alpha" />
        </div>
      </div>
      <table className="table-fixed sm:w-5/6 text-center m-10">
        <thead className="text-center">
          <tr className="h-10 bg-blue-300 border-t border-black">
            <th className="w-1/6">Nomor</th>
            <th className="w-2/5">Mata Pelajaran</th>
            <th className="w-1/6">Hadir</th>
            <th className="w-1/6">Sakit</th>
            <th className="w-1/6">Izin</th>
            <th className="w-1/6">Alpha</th>
          </tr>
        </thead>
        <tbody>
          {
            list
              ? list.length !== 0 ? list.map((obj, position) => (
                <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                  <td>{position + 1}</td>
                  <td className="text-left font-semibold text-lg">{obj.name}</td>
                  <td>{obj.hadir}</td>
                  <td>{obj.sakit}</td>
                  <td>{obj.izin}</td>
                  <td>{obj.alpha}</td>
                </tr>
              )) : (
                <tr>
                  <td colSpan="6" className="text-2xl font-bold h-52">Data kehadiran belum tersedia</td>
                </tr>
              )
              : (
                <tr>
                  <td colSpan="6" className="text-2xl font-bold h-52">Memuat...</td>
                </tr>
              )
          }
        </tbody>
      </table>
    </div>
  );
}
