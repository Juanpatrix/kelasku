import Image from 'next/image';
import { BookmarkIcon } from '@heroicons/react/solid';
import { BookOpenIcon } from '@heroicons/react/outline';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import GraphScore from '../../../components/Graph';
import Dashboard from '../../../libraries/dashboard';
import User from '../../../libraries/user';
import Leaderboard from '../../../libraries/leaderboard';

export default function HomeContainer() {
  const [agenda, setAgenda] = useState([]);
  const [reading, setReading] = useState([]);
  const [user, setUser] = useState([{username: ''}]);
  const [score, setScore] = useState([]);
  const [child, setChild] = useState();
  const [ranking, setRanking] = useState([{id: null}]);
  const [value, setValue] = useState(new Date());

  const loadData = async () => {
    try {
      const token = localStorage.getItem('user');
      const itemSubject = await Dashboard.fetchReadingLatest(token);
      setReading(itemSubject);

      const userTemp = await User.fetchUser(token);
      const child = await Dashboard.fetchStudent(token);
      const leaderboard = await Leaderboard(token, '');
      const position = leaderboard.filter((obj) => obj.totalscore).map((obj, position) => ({
        id: position + 1,
        name: obj.name,
        score: obj.totalscore,
      }));
      setUser(userTemp);
      if (!position) {
        setRanking(position.filter((obj) => obj.name === child[0].name));
      }
      setChild(child[0].name);
      const itemScore = await Dashboard.fetchScoreLatest(token);
      setScore(itemScore.map((obj) => obj.score));
      const itemAgenda = await Dashboard.fetchAgendaLatest(token);
      setAgenda(itemAgenda);
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    loadData();
  }, []);

  return (
    <div className="m-4">
      <div className="text-3xl font-extrabold mb-10">
        <h3>
          {`Selamat datang, ${user[0].username}`}
        </h3>
      </div>
      <div className="grid grid-cols-3 gap-10">
        {/* item 1 */}
        <div className="col-span-3 md:col-span-2">
          <div className="rounded-3xl bg-yellow-400 filter drop-shadow-xl flex p-6">
            <div className="text-white w-2/3 my-auto">
              <h3 className="text-lg md:text-2xl font-medium">Orang hebat bisa melahirkan beberapa karya bermutu tetapi guru yang bermutu dapat melahirkan ribuan orang-orang hebat</h3>
            </div>
            <div className="w-1/2 text-center">
              <Image src="/images/dashboard/teacher.png" width={200} height={200} />

            </div>
          </div>
        </div>
        {/* item 1 */}

        {/* item 2 */}
        <div className="bg-green-200 overflow-hidden row-span-2 rounded-lg filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold text-center mb-10">Kalender</h4>
          <Calendar
            onChange={setValue}
            value={value}
            className="mx-auto"
          />
        </div>
        {/* item 2 */}

        {/* item 3 */}
        <div className="bg-yellow-200 rounded-lg col-span-2 row-span-1 filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold">Agenda Terbaru</h4>
          {/* Item Agenda */}
          <div className="grid grid-cols-3 gap-5">
            {agenda.length === 0 ? (
              <div className="my-5 text-lg col-span-3 text-center font-semibold">
                Tidak ada agenda
              </div>
            ) : agenda.map((obj, position) => (
              <div key={position} className="bg-white m-4 p-3 rounded filter drop-shadow">
                <Link href={`parent/agenda/detail/${obj.id}`}>
                  <a target="_blank">
                    <div className="flex hover:opacity-75 items-center my-2">
                      <div className="bg-blue-300 rounded-full mr-3">
                        <BookmarkIcon className="text-white h-5 w-5 m-3" />
                      </div>
                      {' '}
                      <div>
                        <p className="md:text-xl font-medium">{obj.title}</p>
                        <p className="md:text-lg">{obj.tags}</p>
                      </div>
                    </div>
                  </a>
                </Link>
              </div>
            ))}
          </div>
          {/* Item Agenda */}
        </div>
        {/* item 3 */}

        {/* item 4 */}
        <div className="bg-purple-200 flex flex-col justify-center rounded-lg text-center filter drop-shadow-xl p-2">
          <div className="bg-yellow-600 mx-auto w-max rounded-2xl p-4">
            <Image src="/images/dashboard/trophy.png" height={150} width={150} />
          </div>
          <div className="my-10">
            <h4 className="text-md md:text-2xl font-bold">
              Peringkat
              {' '}
              { ranking.length === 0 ? 'belum diketahui' : ranking[0].id }
            </h4>
            <h5 className="text-md md:text-xl font-bold">
              {child}
            </h5>
            <p>
              { ranking[0].id === 1 ? 'Selamat anak anda juara 1 !!!'
                : 'Ayo dukung dan bimbing anak belajar'}
            </p>
            <Link href="/parent/leaderboard">
              <a>
                <div className="mt-5">
                  <button type="button" className="px-2 py-2 rounded bg-blue-900 hover:bg-blue-800 text-white">Lihat Leaderboard</button>
                </div>
              </a>
            </Link>
          </div>
        </div>
        {/* item 4 */}

        {/* item 5 */}
        <div className="bg-purple-200 col-span-2 rounded-lg filter drop-shadow-xl p-2">
          <div className="bg-white m-4">
            <GraphScore score={score} />
          </div>
        </div>
        {/* item 5 */}

        {/* item 6 */}
        <div className="bg-blue-200 col-span-3 rounded-lg filter drop-shadow-xl p-2">
          <h4 className="text-md md:text-2xl font-bold">Materi Terbaru</h4>
          {/* Item Materi */}
          <div className="grid grid-cols-3">
            {reading.map((obj, position) => (
              <div key={position} className="bg-white my-4 col-span03 mx-11 p-3 rounded filter drop-shadow">
                <div className="flex items-center my-2">
                  <div className="mr-3">
                    <Image src={obj.pic} width={50} height={50} className="rounded-xl" />
                  </div>
                  {' '}
                  <div>
                    <p className="md:text-xl font-semibold">{obj.mapel}</p>
                    <p className="md:text-lg">{obj.name}</p>
                  </div>
                </div>
                <div className="text-right mt-6">
                  <Link href={obj.link}>
                    <a target="_blank">
                      <button type="button" className="px-5 py-1 bg-blue-900 hover:bg-blue-800 text-white rounded">
                        Lihat
                      </button>
                    </a>
                  </Link>
                </div>
              </div>
            ))}
          </div>
          {/* Item Materi */}
        </div>
        {/* item 6 */}
      </div>
    </div>
  );
}
