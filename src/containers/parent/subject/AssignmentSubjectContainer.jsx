/* eslint-disable no-nested-ternary */
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { EyeIcon, PlusIcon, PencilAltIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import BackButton from '@/components/BackButton';
import Class from '../../../libraries/class';

export default function AssignmentSubjectContainer() {
  const [value, setValue] = useState(false);
  const router = useRouter();
  const { id } = router.query;
  const loadSubject = async () => {
    try {
      const token = localStorage.getItem('user');
      if (id) {
        const itemSubject = await Class.fetchAssignmentParent(token, id);
        setValue(itemSubject);
      }
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadSubject();
  }, [router.isReady]);
  return (
    <div className="m-2">
      <div className="ml-10 mt-10">
        <BackButton />
      </div>
      <h2 className="text-center font-extrabold text-4xl mt-8">
        Tugas
      </h2>
      <table className="table-fixed w-5/6 mx-auto text-center mt-16">
        <thead>
          <tr className="h-14 bg-blue-300 border-t border-black">
            <th className="w-1/12 text-lg text-center">No</th>
            <th className="w-1/6 text-lg text-center">Tanggal</th>
            <th className="w-1/2">Nama Tugas</th>
            <th className="w-1/3">Score</th>
          </tr>
        </thead>
        <tbody>
          {value
            ? value.length !== 0 ? value.map((obj, position) => (
              <tr key={position} className="border-2 bg-blue-100 border-gray-300 h-16">
                <td>{position + 1}</td>
                <td>{obj.createdAt.substring(0, 10).split('-').reverse().join('-')}</td>
                <td className="text-left text-lg font-semibold">{obj.topic}</td>
                <td className="text-center text-lg font-semibold">{!obj.score ? 'Belum Dikoreksi' : obj.score}</td>
              </tr>
            ))
              : (
                <tr>
                  <td colSpan="5" className="text-2xl font-bold h-52">Tidak Ada Tugas</td>
                </tr>
              )
            : (
              <tr>
                <td colSpan="5" className="text-2xl font-bold h-52">Memuat...</td>
              </tr>
            )}
        </tbody>
      </table>
    </div>
  );
}
