import Head from 'next/head';
import { useRouter } from 'next/router';
import {createContext, useContext, useEffect} from 'react';
import '../styles/globals.css';
import Authentication from '../libraries/authentication';

const AuthContext = createContext();
export const useAuth = () => useContext(AuthContext);

function MyApp({Component, pageProps}) {
  return (
    <>
      <Head>
        <meta name="description" content="KelasKu merupakan aplikasi monitor Orang Tua terhadap perkembangan belajar murid" />
        <link rel="icon" href="/favicon/favicon-32x32.png" />
        <link rel="icon" href="/favicon/favicon-16x16.png" />
        <link rel="icon" href="/favicon/apple-touch-icon.png" />
        <link rel="icon" href="/favicon/android-chrome-192x192.png" />
        <link rel="icon" href="/favicon/favicon-32x32.png" />
      </Head>
      {Component.auth ? (
        <Auth>
          <Component {...pageProps} />
        </Auth>
      ) : (
        <Component {...pageProps} />
      )}
    </>
  );
}




function Auth({ children }) {
  const router = useRouter();
  useEffect(async () => {
    const isUser = localStorage.getItem('user');
    try {
      const result = await Authentication(isUser);
      const path = router.pathname.split('/').join('/').replace(/student|teacher|parent/gi, '');
      switch (result.role_id) {
        case 1:
          if (router.pathname === `/student${path.replace('/', '')}`) return children;
          router.push('/student');
          break;
        case 2:
          if (router.pathname === `/teacher${path.replace('/', '')}`) return children;
          router.push('/teacher');
          break;
        case 3:
          if (router.pathname === `/parent${path.replace('/', '')}`) return children;
          router.push('/parent');
          break;
        default:
          router.push('/login');
      }
    } catch (error) {
      router.push('/login');
      return children;
    }
  });
  return children;
}

export default MyApp;
