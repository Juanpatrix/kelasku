import LoginContainer from '@/containers/home/LoginContainer';

export default function LoginPage() {
  return (
    <div className="flex min-h-screen">
      <LoginContainer />
    </div>
  );
}
