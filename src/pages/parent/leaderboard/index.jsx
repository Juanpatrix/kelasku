import LeaderboardContainer from '@/containers/leaderboard/LeaderboardContainer';
import ParentLayout from '../../../layouts/parent/index';

export default function LeaderboardPage() {
  return (
    <ParentLayout page="Papan Peringkat">
      <LeaderboardContainer />
    </ParentLayout>
  );
}

LeaderboardPage.auth = true;
