import ParentLayout from '../../layouts/parent/index';
import HomeContainer from "@/containers/parent/dashboard/HomeContainer";

export default function DashboardPage() {
  return (
    <ParentLayout page="Beranda">
      <HomeContainer />
    </ParentLayout>
  );
}

DashboardPage.auth = true;
