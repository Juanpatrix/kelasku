import DetailAgendaContainer from '@/containers/parent/agenda/DetailAgendaContainer';
import ParentLayout from '../../../../layouts/parent/index';

export default function DetailAgendaPage() {
  return (
    <ParentLayout page="Agenda">
      <DetailAgendaContainer />
    </ParentLayout>
  );
}

DetailAgendaPage.auth = true;

