import ListAgendaContainer from '@/containers/parent/agenda/ListAgendaContainer';
import ParentLayout from '../../../layouts/parent/index';

export default function ListAgendaPage() {
  return (
    <ParentLayout page="Agenda">
      <ListAgendaContainer />
    </ParentLayout>
  );
}

ListAgendaPage.auth = true;
