import AssignmentSubjectContainer from '@/containers/parent/subject/AssignmentSubjectContainer';
import ParentLayout from '../../../../../layouts/parent/index';

export default function AssignmentSubjectPage() {
  return (
    <ParentLayout page="Kelas">
      <AssignmentSubjectContainer />
    </ParentLayout>
  );
}

AssignmentSubjectPage.auth = true;
