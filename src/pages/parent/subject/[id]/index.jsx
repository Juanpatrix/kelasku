import ParentLayout from '../../../../layouts/parent/index';
import DetailSubjectContainer from '@/containers/parent/subject/DetailSubjectContainer';

export default function DetailSubjectPage() {
  return (
    <ParentLayout page="Kelas">
      <DetailSubjectContainer />
    </ParentLayout>
  );
}

DetailSubjectPage.auth = true;
