import ParentLayout from '../../../../../layouts/parent/index';
import ReadingSubjectContainer from '@/containers/parent/subject/ReadingSubjectContainer';

export default function ReadingSubjectPage() {
  return (
    <ParentLayout page="Kelas">
      <ReadingSubjectContainer />
    </ParentLayout>
  );
}

ReadingSubjectPage.auth = true;
