import ParentLayout from '../../../layouts/parent/index';
import ListSubjectContainer from '@/containers/parent/subject/ListSubjectContainer';

export default function ListSubjectPage() {
  return (
    <ParentLayout page="Kelas">
      <ListSubjectContainer />
    </ParentLayout>
  );
}

ListSubjectPage.auth = true;
