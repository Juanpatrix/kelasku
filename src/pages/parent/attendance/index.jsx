import AttendanceContainer from '@/containers/parent/attendance/AttendanceContainer';
import ParentLayout from '../../../layouts/parent/index';

export default function AttendancePage() {
  return (
    <ParentLayout page="Absensi">
      <AttendanceContainer />
    </ParentLayout>
  );
}

AttendancePage.auth = true;
