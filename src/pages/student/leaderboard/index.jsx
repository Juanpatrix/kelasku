import LeaderboardContainer from '@/containers/leaderboard/LeaderboardContainer';
import StudentLayout from '../../../layouts/student/index';

export default function LeaderboardPage() {
  return (
    <StudentLayout page="Papan Peringkat">
      <LeaderboardContainer />
    </StudentLayout>
  );
}

LeaderboardPage.auth = true;

