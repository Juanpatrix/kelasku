import StudentLayout from '../../../../../layouts/student/index';
import ReadingSubjectContainer from '@/containers/student/subject/ReadingSubjectContainer';

export default function ReadingSubjectPage() {
  return (
    <StudentLayout page="Kelas">
      <ReadingSubjectContainer />
    </StudentLayout>
  );
}

ReadingSubjectPage.auth = true;
