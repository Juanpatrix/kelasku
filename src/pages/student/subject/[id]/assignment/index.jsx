import AssignmentSubjectContainer from '@/containers/student/subject/AssignmentSubjectContainer';
import StudentLayout from '../../../../../layouts/student/index';

export default function AssignmentSubjectPage() {
  return (
    <StudentLayout page="Kelas">
      <AssignmentSubjectContainer />
    </StudentLayout>
  );
}

AssignmentSubjectPage.auth= true;
