import SubmitAssignmentContainer from '@/containers/student/subject/SubmitAssignmentContainer';
import StudentLayout from '../../../../../layouts/student/index';

export default function SubmitAssignmentPage() {
  return (
    <StudentLayout page="Kelas">
      <SubmitAssignmentContainer />
    </StudentLayout>
  );
}

SubmitAssignmentPage.auth= true;
