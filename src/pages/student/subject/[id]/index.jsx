import StudentLayout from '../../../../layouts/student/index';
import DetailSubjectContainer from '@/containers/student/subject/DetailSubjectContainer';

export default function DetailSubjectPage() {
  return (
    <StudentLayout page="Kelas">
      <DetailSubjectContainer />
    </StudentLayout>
  );
}

DetailSubjectPage.auth = true;
