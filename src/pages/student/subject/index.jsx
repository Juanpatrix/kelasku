import ListSubjectContainer from '@/containers/student/subject/ListSubjectContainer';
import StudentLayout from '../../../layouts/student/index';

export default function ListSubjectPage() {
  return (
    <StudentLayout page="Kelas">
      <ListSubjectContainer />
    </StudentLayout>
  );
}

ListSubjectPage.auth = true;
