import StudentLayout from '../../layouts/student/index';
import HomeContainer from "@/containers/student/dashboard/HomeContainer";

export default function DashboardPage() {
  return (
    <StudentLayout page="Beranda">
      <HomeContainer />
    </StudentLayout>
  );
}

DashboardPage.auth = true;
