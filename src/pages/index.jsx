import {
  ChatAltIcon, ViewBoardsIcon, ClipboardListIcon, AcademicCapIcon,
} from '@heroicons/react/outline';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { Dialog, Transition } from '@headlessui/react';
import { Fragment, useState } from 'react';
import parent from '../../public/images/home/parent.png';
import kelas from '../../public/images/home/class.png';

const features = [
  {
    name: 'Kelas',
    description:
    'Tempat proses belajar anak dengan suasana di rumah',
    icon: AcademicCapIcon,
  },
  {
    name: 'Agenda',
    description:
    'Diskusi Guru dengan Orang tua untuk memantau perkembangan belajar anak',
    icon: ChatAltIcon,
  },
  {
    name: 'Absensi',
    description:
    'Pemantauan kehadiran siswa dengan kehadiran, sakit, izin, dan alpha',
    icon: ClipboardListIcon,
  },
  {
    name: 'Papan Peringkat',
    description:
    'Tempat melihat ranking anak di kelas',
    icon: ViewBoardsIcon,
  },
];

export default function Home() {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }
  return (
    <>
      <div className="mt-8 bg-gray-50">
        {/* Hero */}
        <div className="flex justify-center">
          <div className="md:w-1/2 text-center p-16">
            <h1 className="text-4xl font-bold md:text-left mb-4">KelasKu</h1>
            <p className="text-gray-400 mb-4 md:text-left text-lg md:text-xl">Memantau kondisi perkembangan belajar anak</p>
            <button className="hover:bg-yellow-300 active:bg-yellow-700 bg-yellow-400 w-1/2 py-2 px-4 text-white rounded mt-10" type="button" onClick={() => router.push('/login')}>Login</button>
          </div>
          <div className="w-1/2 hidden md:block md:mr-10">
            <Image layout="responsive" height={50} width={75} src={kelas} />
          </div>
        </div>
        {/* Hero */}
        {/* Survey */}
        <div className="my-12">
          <div className="flex flex-col-reverse md:flex-row-reverse ">
            <div className="md:w-1/2 p-8">
              <h1 className="text-2xl md:mt-6 md:text-4xl font-bold mb-4">Tentang Kami</h1>
              <p className="text-gray-400 mb-4 text-sm md:text-lg">Pandemi ini, ada beberapa orang tua yang dapat mengerjakan pekerjaan dari rumah saja sehingga intensitas waktu bertemu anak sangat banyak. Ada orang tua yang tetap harus bekerja di luar rumah untuk bisa mencukupi kebutuhan hidup mereka sehari-hari. Studi penelitian menunjukan bahwa pembelajaran online mengganggu, menantang dan cukup memakan waktu. Peran orang tua dan dewasa sekitar anak sangat diperlukan, seperti pengawasan apa saja yang dibuka anak. Oleh karena itu, KelasKu hadir untuk membantu mengawasi perkembangan belajar anak.</p>
            </div>
            <div className="md:w-1/2 m-14">
              <Image layout="responsive" height={50} width={65} src={parent} />
            </div>
          </div>
        </div>
        {/* Survey */}
        {/* Fitur */}
        <div className="m-10 flex md:flex-row flex-col-reverse rounded-3xl p-16 bg-gradient-to-r from-blue-300 via-gray-200 to-yellow-100">
          <div className="md:w-1/2 md:mt-0 mt-10 text-center md:text-left">
            <h1 className="md:text-4xl text-2xl font-medium md:font-bold mb-8 md:mr-8">Ayo segera bergabung dan pantau perkembangan anak buah hati anda</h1>
            <button className="bg-yellow-400 py-2 px-4 text-white rounded" onClick={openModal} type="button">Daftar Sekarang</button>

          </div>
          <div className="md:w-1/2 md:space-y-0 space-y-6 md:grid md:grid-cols-2 md:gap-x-8 md:gap-y-10">
            {features.map((feature) => (
              <div key={feature.name} className="relative">
                <div className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-white text-yellow-500">
                  <feature.icon className="h-6 w-6" aria-hidden="true" />
                </div>
                <p className="ml-16 text-lg leading-6 font-medium text-gray-900">{feature.name}</p>
                <div className="mt-2 ml-16 text-base text-gray-500">{feature.description}</div>
              </div>
            ))}
          </div>
        </div>
        {/* Fitur */}

      </div>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto bg-opacity-70 bg-black"
          onClose={closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900"
                >
                  Pendaftaran Online
                </Dialog.Title>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">
                    Kontak: kelasku@gmail.com
                  </p>
                </div>

                <div className="mt-4">
                  <button
                    type="button"
                    className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-900 bg-red-100 border border-transparent rounded-md hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                    onClick={closeModal}
                  >
                    Tutup
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
