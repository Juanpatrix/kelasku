import TeacherLayout from '../../../../layouts/teacher/index';
import DetailSubjectContainer from '@/containers/teacher/subject/DetailSubjectContainer';

export default function DetailSubjectPage() {
  return (
    <TeacherLayout page="Kelas">
      <DetailSubjectContainer />
    </TeacherLayout>
  );
}

DetailSubjectPage.auth = true;