import CorrectionAssignmentContainer from '@/containers/teacher/subject/CorrectionAssignmentContainer';
import TeacherLayout from '../../../../../layouts/teacher/index';

export default function CorrectionAssignmentPage() {
  return (
    <TeacherLayout page="Kelas">
      <CorrectionAssignmentContainer />
    </TeacherLayout>
  );
}

CorrectionAssignmentPage.auth=true;
