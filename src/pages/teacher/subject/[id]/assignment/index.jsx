import AssignmentSubjectContainer from '@/containers/teacher/subject/AssignmentSubjectContainer';
import TeacherLayout from '../../../../../layouts/teacher/index';

export default function AssignmentSubjectPage() {
  return (
    <TeacherLayout page="Kelas">
      <AssignmentSubjectContainer />
    </TeacherLayout>
  );
}

AssignmentSubjectPage.auth = true;