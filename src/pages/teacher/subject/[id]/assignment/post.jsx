import AddAssignmentContainer from '@/containers/teacher/subject/AddAssignmentContainer';
import TeacherLayout from '../../../../../layouts/teacher/index';

export default function AssignmentSubjectPage() {
  return (
    <TeacherLayout page="Kelas">
      <AddAssignmentContainer />
    </TeacherLayout>
  );
}
AssignmentSubjectPage.auth = true;
