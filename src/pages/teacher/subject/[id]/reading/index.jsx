import TeacherLayout from '../../../../../layouts/teacher/index';
import ReadingSubjectContainer from '@/containers/teacher/subject/ReadingSubjectContainer';

export default function ReadingSubjectPage() {
  return (
    <TeacherLayout page="Kelas">
      <ReadingSubjectContainer />
    </TeacherLayout>
  );
}

ReadingSubjectPage.auth = true;
