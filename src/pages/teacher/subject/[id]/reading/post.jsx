import TeacherLayout from '../../../../../layouts/teacher/index';
import AddReadingContainer from '@/containers/teacher/subject/AddReadingContainer';

export default function AddReadingPage() {
  return (
    <TeacherLayout page="Kelas">
      <AddReadingContainer />
    </TeacherLayout>
  );
}

AddReadingPage.auth = true;
