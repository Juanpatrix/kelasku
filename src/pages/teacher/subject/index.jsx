import TeacherLayout from '../../../layouts/teacher/index';
import ListSubjectContainer from '@/containers/teacher/subject/ListSubjectContainer';

export default function ListSubjectPage() {
  return (
    <TeacherLayout page="Kelas">
      <ListSubjectContainer />
    </TeacherLayout>
  );
}

ListSubjectPage.auth = true;
