import AddAgendaContainer from '@/containers/teacher/agenda/AddAgendaContainer';
import TeacherLayout from '../../../layouts/teacher/index';

export default function AddAgendaPage() {
  return (
    <TeacherLayout page="Agenda">
      <AddAgendaContainer />
    </TeacherLayout>
  );
}
AddAgendaPage.auth = true;
