import TeacherLayout from '../../../layouts/teacher/index';
import ListAgendaContainer from '@/containers/teacher/agenda/ListAgendaContainer';

export default function ListAgendaPage() {
  return (
    <TeacherLayout page="Agenda">
      <ListAgendaContainer />
    </TeacherLayout>
  );
}

ListAgendaPage.auth = true;
