import DetailAgendaContainer from '@/containers/teacher/agenda/DetailAgendaContainer';
import TeacherLayout from '../../../../layouts/teacher/index';

export default function DetailAgendaPage() {
  return (
    <TeacherLayout page="Agenda">
      <DetailAgendaContainer />
    </TeacherLayout>
  );
}

DetailAgendaPage.auth = true;

