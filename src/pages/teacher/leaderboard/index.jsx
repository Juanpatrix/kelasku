import TeacherLayout from '../../../layouts/teacher/index';
import LeaderboardContainer from '@/containers/leaderboard/LeaderboardContainer';

export default function LeaderboardPage() {
  return (
    <TeacherLayout page="Papan Peringkat">
      <LeaderboardContainer />
    </TeacherLayout>
  );
}

LeaderboardPage.auth = true;

