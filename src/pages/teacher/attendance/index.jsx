import TeacherLayout from '../../../layouts/teacher/index';
import ListSubjectContainer from '@/containers/teacher/attendance/ListSubjectContainer';

export default function AttendancePage() {
  return (
    <TeacherLayout page="Absensi">
      <ListSubjectContainer />
    </TeacherLayout>
  );
}

AttendancePage.auth = true;
