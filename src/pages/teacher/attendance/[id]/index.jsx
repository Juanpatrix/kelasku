import TeacherLayout from '../../../../layouts/teacher/index';
import ListAttendanceContainer from '@/containers/teacher/attendance/ListAttendanceContainer';

export default function ListAttendaPage() {
  return (
    <TeacherLayout page="Absensi">
      <ListAttendanceContainer />
    </TeacherLayout>
  );
}