import TeacherLayout from '../../../../../layouts/teacher/index';
import AttendanceContainer from '@/containers/teacher/attendance/AttendanceContainer';

export default function ListAttendancePage() {
  return (
    <TeacherLayout page="Absensi">
      <AttendanceContainer />
    </TeacherLayout>
  );
}