import TeacherLayout from '../../layouts/teacher/index';
import HomeContainer from "@/containers/teacher/dashboard/HomeContainer";

export default function DashboardPage() {
  return (
    <TeacherLayout page="Beranda">
      <HomeContainer />
    </TeacherLayout>
  );
}

DashboardPage.auth = true;
