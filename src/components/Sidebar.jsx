import { Transition } from '@headlessui/react';
import { useState, useEffect } from 'react';
import { ChevronDoubleLeftIcon, UserCircleIcon } from '@heroicons/react/solid';
import { LogoutIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';
import User from '../libraries/user';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Sidebar(props) {
  const [value, setValue] = useState([]);
  const router = useRouter();
  const [open, setOpen] = useState(true);
  const openBar = () => {
    setOpen(!open);
  };

  const loadUser = async () => {
    try {
      const token = localStorage.getItem('user');
      const user = await User.fetchUser(token);
      setValue(user.reduce((obj, item) => ({...obj, name: item.username, role: item.name}), {}));
    } catch (error) {
      Promise.reject(error);
    }
  };
  useEffect(() => {
    if (!router.isReady) return;
    loadUser();
  }, [router.isReady]);

  const logOut = () => {
    localStorage.removeItem('user');
    router.replace('/login');
  };

  return (
    <>
      <Transition
        show={open}
        enter="transition ease-in-out duration-200 transform"
        enterFrom="-translate-x-full"
        enterTo="translate-x-0"
        leave="transition ease-in-out duration-300 transform"
        leaveFrom="translate-x-0"
        leaveTo="-translate-x-full"
        className="md:w-1/3 h-screen md:w-1/2 bg-gray-900 md:sticky top-0"
      >
        <div className="hidden md:block">
          {value.role === 'Guru' ? (
            <div className="my-6 mx-auto w-20 h-20 flex justify-center text-yellow-500 items-center rounded-full bg-white">
              <UserCircleIcon />
            </div>
          ) : value.role === 'Orangtua' ? (
            <div className="my-6 mx-auto w-20 h-20 flex justify-center text-green-500 items-center rounded-full bg-white">
              <UserCircleIcon />
            </div>
          ) : value.role === 'Murid' ? (
            <div className="my-6 mx-auto w-20 h-20 flex justify-center text-blue-500 items-center rounded-full bg-white">
              <UserCircleIcon />
            </div>
          ) : (
            <div className="mt-16 mx-auto mb-6 w-20 h-20 flex justify-center text-gray-100 items-center rounded-full bg-white">
              <UserCircleIcon />
            </div>
          )}
          <div className="mb-12 text-center text-lg font-semibold">
            <div className="text-white">
              {value.name}
            </div>
            <div className="text-white">
              {value.role}
            </div>
          </div>

          <div className="ml-5">
            {props.itemNav.map((item) => (

              <a
                key={item.name}
                href={item.href}
                className={classNames(
                  (props.page === item.name) ? 'text-white' : 'text-gray-500 hover:text-white',
                  'block px-5 py-2 font-medium flex items-center my-2',
                )}
                aria-current={(props.page === item.name) ? 'page' : undefined}
              >
                {item.icon}
                {' '}
                {item.name}
              </a>
            ))}
          </div>
          <div className="flex mt-10 justify-center items-center">
            <button type="button" onClick={logOut} className="flex text-white">
              <LogoutIcon className="h-8 w-8 mr-2" />
              <p className="text-lg">Keluar</p>
            </button>
          </div>
        </div>
      </Transition>
      <div className="h-screen sticky top-0 hidden md:block">
        <button type="button" onClick={openBar}>
          <ChevronDoubleLeftIcon className={classNames(
            open ? 'transform rotate-180 ' : '',
            'h-14 w-14 bg-gray-300 rounded text-black',
          )}
          />
        </button>
      </div>
    </>
  );
}
