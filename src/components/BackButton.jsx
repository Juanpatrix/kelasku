import { useRouter } from 'next/router';

export default function BackButton() {
  const router = useRouter();
  const backPage = () => {
    router.back();
  };
  return (
    <button className="bg-blue-900 hover:bg-blue-800 text-white px-10 py-2 rounded" onClick={backPage} type="button">
      Kembali
    </button>
  );
}
