import { Disclosure } from '@headlessui/react';
import { ViewListIcon } from '@heroicons/react/solid';
import { useRouter } from 'next/router';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Navbar(props) {
  const router = useRouter();
  const logOut = () => {
    localStorage.removeItem('user');
    router.replace('/login');
  };
  return (
    <div className="bg-white sticky top-0 z-10  min-w-full filter drop-shadow-xl md:hidden">
      <Disclosure>
        {({ open }) => (
          <>
            <Disclosure.Button className="float-right">
              <ViewListIcon className="h-14 w-14" />
            </Disclosure.Button>
            <Disclosure.Panel>
              <div className="px-2 pb-3 flex">
                <div className="mx-2 mt-12">
                  {props.itemNav.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      className={classNames(
                        (props.page === item.name) ? 'text-black' : 'text-gray-400 hover:text-black',
                        'block font-medium py-2 my-2',
                      )}
                      aria-current={(props.page === item.name) ? 'page' : undefined}
                    >
                      {item.name}
                    </a>
                  ))}
                  <div>
                    <a onClick={logOut} className="text-gray-400 hover:text-black block font-medium py-2 my-2">
                      Keluar
                    </a>
                  </div>
                </div>
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </div>
  );
}
